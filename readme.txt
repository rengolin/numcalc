NumCalc - Numerical Calculus C++ Library

1. NumCalc

NumCalc is a set of classes to perform numerical calculus
algorithms on user defined functions. The main aim are
easy to read code, performance and stability, although
the former is a bit more important.

Not all classes have all implementations, but the aim is first
to have something that works accurately, then having more methods
to compare against, then to explain in details each inner working.

Adding unit tests is how I make sure the expected functionality
works, it's how I design the classes and how it ultimately gets
validated before every commit.

The classes currently implemented are:

* Derivate: Simple numerical derivative of equations.
* Integrate: Numerical integration.
* Interpolate: Cubic spline interpolation.
* MatrixSolver: Solves system of equations.
* Poly: Evaluate polynomials efficiently.
* RootFinder: Finds the roots of equations.
* ODE: Differential equation solver.
* BestFit: Tries to fit polynomials, trigonometric or exponential
  curves to a set of points.
* Chain: Allows one to create a Markov chaing and then randomly
  walk through it.
* EquationParser: Parses simple equations and evaluates them at
  requested values for the variables.

2. How to use

Each class has a tool that uses for some random input data,
usually a text file in some pre-determined format (see the
directory 'examples' in the source tree for more details).

Each class also has a unit test, which runs when you "make"
the project, but also works on "make check". They should all
print "PASS".

Both tools and tests will give you a number of ideas on how to
use, what are the limitations and how to improve later on.

3. Tools in progress

The equation parser is working, but still has a number of draw
backs and inefficiencies. It needs to improve a lot to be a
tool on its own, but can already be used by other tools to carry
on more complex equations than polynomials.

Further manpulation of the AST would allow optimisation, floating-point
accuracy improvement and even symbolic derivation.

4. Improvements

* Better output

Some of the tools output gnuplot scripts to see the results.
While this is pretty cool, it's far from being user-friendly.
My idea is to run gnuplot directly, probably by saving the
data files and printing out the script to STDOUT, so users can:
$ cat data | ./tool | gnuplot

* Better input

The input file format has no verification at all and rely on
header lines to compute the number of items. This works, but it's
bad form. A plan is to add some magical header (shebang line)
to make sure people don't use the wrong data files with tools,
use getline() + stringstreamer, and make sure the values make
sense as they're read.

That'd probably need a common library to all tools, and that's the
reason why I didn't do that yet. I am still focusing on improving
the algorithms.

* More pedestrian algorithms

In order to compare performance, we need more pedestrian algorithms
and run some tests to show the difference. The root-finder has this
well done, but I got lazy afterwards. It'd also be good to have it
separate from the test-suite.

* More comments

Since the objective of this library is educational, having more
comments and some explanation on what's going on would be better.
Even though the main classes do refer to their text-book chapters,
I can't assume everyone has those books at home.

The plot fit (BestFit.h) and the root-finding algorithms have some
good examples, but it'd be good to have that for all the others.

5. Contribution

If you want to use the classes in your project, please let me know,
as I'd be glad to advertise your project here.

If you find bugs or want to improve the classes, pull requests are
all you need. I just ask that you add unit tests for every change
and do a "make check" before sending the request.

6. License

The license is Apache 2.0, as it means this can be used as a library
in virtually any project out there. It's also compatible with most
other open source licenses. If there is any problem with the license,
please let me know and I can add special clauses.

I'm not a lawyer, but I also don't want people to not contribute
due to license issues.

Renato Golin <rengolin at systemcall dot eu>
