#include <iostream>
#include <fstream>
#include "BestFit.h"

using namespace std;
using namespace numcalc;

int main() {
  unsigned data;
  cin >> data;
  if (data > 100) {
    cerr << "ERROR: Best Fit file format:" << endl;
    cerr << "datapoints (int)" << endl;
    cerr << "x1 y1 x2 y2 ... x(data) y(data) (double data points)" << endl;
    return 1;
  }

  points_t data_points;
  double x, y;
  for (unsigned i=0; i<data; i++) {
    cin >> x >> y;
    data_points.push_back(point_t(x, y));
  }

  BestFit fit(data_points);
  Fit *best = fit.getBest();

  // Gnuplot output
  fstream pfs("data.out", fstream::out);
  for (unsigned i=0; i<data_points.size(); i++) {
    point_t p = data_points[i];
    pfs << p.x << " " << p.y << endl;
  }
  pfs.close();

  fstream script("script.gp", fstream::out);
  script << "# Gnuplot script" << endl;
  script << "set terminal x11 background rgb 'white'" << endl;
  script << "set title 'Simple Plots' font ',20'" << endl;
  script << "set key left box" << endl;
  script << "plot 'data.out' title 'Points', \\" << endl;
  script << "     " << PrintEquation(best) << " title 'Best Fit'" << endl;
  script << "set xlabel '" << PrintEquation(best) << "'" << endl;
  script << "pause mouse key" << endl;
  script.close();

  cout << "Gnuplot script created. To see the results, type:" << endl;
  cout << "$ gnuplot script.gp" << endl;


  return 0;
}
