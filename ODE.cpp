#include "ODE.h"

using namespace numcalc;

point_t
ODE::next(point_t t0, unsigned steps) {
  double h2 = step/2;
  while (steps--) {
    double k1 = (*f)(t0);
    double k2 = (*f)(point_t(t0.x + h2, t0.y + h2*k1));
    double k3 = (*f)(point_t(t0.x + h2, t0.y + h2*k2));
    double k4 = (*f)(point_t(t0.x + step, t0.y + step*k3));
    t0.x += step;
    t0.y += step/6 * (k1 + 2*k2 + 2*k3 + k4);
  }
  return t0;
}

point_t
ODE2::next(point_t t0, unsigned steps) {
  double h2 = step/2;
  while (steps--) {
    double k1 = (*fx)(t0);
    double l1 = (*fy)(t0);
    double k2 = (*fx)(point_t(t0.x + h2*k1, t0.y + h2*l1));
    double l2 = (*fy)(point_t(t0.x + h2*k1, t0.y + h2*l1));
    double k3 = (*fx)(point_t(t0.x + h2*k2, t0.y + h2*l2));
    double l3 = (*fy)(point_t(t0.x + h2*k2, t0.y + h2*l2));
    double k4 = (*fx)(point_t(t0.x + step*k3, t0.y + step*l3));
    double l4 = (*fy)(point_t(t0.x + step*k3, t0.y + step*l3));
    t0.x += step/6 * (k1 + 2*k2 + 2*k3 + k4);
    t0.y += step/6 * (l1 + 2*l2 + 2*l3 + l4);
  }
  return t0;
}
