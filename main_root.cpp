#include <iostream>
#include <fstream>
#include <vector>
#include "RootFinder.h"
#include "Poly.h"

using namespace std;
using namespace numcalc;

// Capture lambdas don't cast to function pointers
// We need a global variable. FIXME: Make Poly better.
Poly *poly;

// Usage: cat equation.txt | root
// Returns root nearest to starting points
// Only works with polynomials, for now
int main() {
  unsigned degree, roots;
  cin >> degree >> roots;
  if (degree > 10 || roots == 0 || roots > 10) {
    cerr << "ERROR: Equation file format:" << endl;
    cerr << "degree num_roots (int int)" << endl;
    cerr << "c0 c1 c2 ... c(degree) (double coefs)" << endl;
    cerr << "r1 r2 r3 ... r(roots) (double root points)" << endl;
    return 1;
  }

  vector<double> coefs(degree+1);
  double value;
  for (unsigned i=0; i<=degree; i++) {
    cin >> value;
    coefs[i] = value;
  }
  poly = new Poly(coefs);

  vector<double> rootps(roots);
  for (unsigned i=0; i<roots; i++) {
    cin >> value;
    rootps[i] = value;
  }

  // compile with c++11 support
  RootFinder rf([](double x)->double { return (*poly)(x);});

  for (unsigned i=0; i<roots; i++)
      cout << "Root near (" << rootps[i] << "): "
                            << rf.find(rootps[i]) << endl;
  delete poly;
  return 0;
}
