#include <iostream>
#include <fstream>
#include <vector>
#include "Poly.h"

using namespace std;
using namespace numcalc;

// Usage: cat poly.txt | poly
// Returns points from A to B in Delta increments
int main() {
  unsigned degree;
  double start, stop, delta;
  cin >> degree >> start >> stop >> delta;
  if (degree > 10 || start >= stop || delta > (stop-start)) {
    cerr << "ERROR: Polynomial file format:" << endl;
    cerr << "degree start stop delta (int double double double)" << endl;
    cerr << "c1 c2 c3 ... c(degree)" << endl;
    return 1;
  }

  vector<double> coefs(degree+1);
  double value;
  for (unsigned i=0; i<=degree; i++) {
    cin >> value;
    coefs[i] = value;
  }

  Poly poly(coefs);

  for (double cur=start; cur<stop; cur+=delta)
    cout << "(" << cur << ", " << poly(cur) << ")" << endl;

  return 0;
}
