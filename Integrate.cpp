#include <cassert>
#include <cmath>
#include "Integrate.h"
#include "Math.h"

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

double
Integrate::sum (double a, double b) {
  unsigned d = depth;
  // Check range
  assert(a < b);

  error = 1; // Dummy value not zero

  // First sum
  double fa = (*f)(a);
  double fb = (*f)(b);
  DEBUG("\t\tf(a, b) = (" << fa << ", " << fb << ")");
  double sum = ( (fa + fb) * (b - a) ) / 2;
  DEBUG("\t\tfirst sum = (" << sum << ")");

  // Iterate until sum is fine enough
  double tmp, chunk, x, oldSum = 0;
  unsigned it = 2;
  while (d--) {
    // Chunk size
    chunk = (b - a) / it;
    DEBUG("\t\t\tchunk = " << chunk);
    // First step
    x = a + chunk / 2;
    DEBUG("\t\t\tstart at = " << x);

    // Iterate through all chunks
    tmp = 0;
    for (unsigned j = 0; j < it; j++, x+= chunk)
      tmp += (*f)(x);

    // Actual sum
    oldSum = sum;
    sum = ( ((b - a) * tmp) / it);
    DEBUG("\t\t\tnew sum = " << sum);

    // Error
    error = pow(chunk, (double) (2 * it + 1));
    DEBUG("\t\t\terror = " << error);
    if (isZero(error, 10e-8))
      break;

    // Stable
    if (isZero(std::abs(oldSum - sum)))
      break;

    // Check if values still valid
    assert(!std::isnan(sum) && !std::isinf(sum));

    it <<= 1;
  }

  return sum;
}

} // namespace numcalc
