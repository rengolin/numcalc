#include <iostream>
#include <fstream>
#include "Interpolate.h"

using namespace std;
using namespace numcalc;

// Usage: cat data.txt | interpolate
// Returns coefficients for all ranges
// If a points are requested, returns the value of Y on each X
int main () {
  unsigned data;
  cin >> data;
  if (data > 10) {
    cerr << "ERROR: Equation file format:" << endl;
    cerr << "datapoints (int)" << endl;
    cerr << "x1 y1 x2 y2 ... x(data) y(data) (double data points)" << endl;
    return 1;
  }

  vector<point_t> data_points;
  vector<double> req_points;
  double x, y, minx=0.0, maxx=0.0;
  bool first = true;
  for (unsigned i=0; i<data; i++) {
    cin >> x >> y;
    data_points.push_back(point_t(x, y));
    if (first) {
      minx = maxx = x;
      first = false;
    } else {
      minx = std::min(minx, x);
      maxx = std::max(maxx, x);
    }
  }

  Interpolate ip (data_points);

  // Gnuplot output
  fstream pfs("data.out", fstream::out);
  for (unsigned i=0; i<data_points.size(); i++) {
    point_t p = data_points[i];
    pfs << p.x << " " << p.y << endl;
  }
  pfs.close();

  fstream ipfs("graph.out", fstream::out);
  for (double t=minx; t < maxx; t += 0.05) {
    point_t p = ip.getPoint(t);
    ipfs << p.x << " " << p.y << endl;
  }
  ipfs.close();

  fstream script("script.gp", fstream::out);
  script << "# Gnuplot script" << endl;
  script << "set terminal x11 background rgb 'white'" << endl;
  script << "set title 'Simple Plots' font ',20'" << endl;
  script << "set key left box" << endl;
  script << "plot 'data.out' title 'Points', \\" << endl;
  script << "     'graph.out' title 'Interpolation' with line lt 2" << endl;
  script << "pause mouse key" << endl;
  script.close();

  cout << "Gnuplot script created. To see the results, type:" << endl;
  cout << "$ gnuplot script.gp" << endl;

  return 0;
}
