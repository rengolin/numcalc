#include <cassert>
#include <cmath>
#include "EquationParser.h"

using namespace std;
using namespace numcalc;

// =========================================== VARIABLE

double VariablePayload::get(const std::string &name) {
  if (ty == One)
    return Single;

  if (ty == Many) {
    assert (!name.empty() && "more than one variable expected");
    auto it = Multi->find(name);
    if (it != Multi->end())
      return (*it).second;
    else
      assert(0 && "variable not found");
  }

  // ty = none, shouldn't be here
  assert (0 && "no variable to get");
}

// =========================================== NODE

Node::Node(double val) : lhs(nullptr), rhs(nullptr) {
  this->type = ND_CONST;
  this->value = val;
}

Node::Node(string name) : lhs(nullptr), rhs(nullptr) {
  this->type = ND_VAR;
  this->name = name;
}

Node::Node(node_t type, Node *val) : lhs(nullptr) {
  switch(type) {
  default:
    assert(0 && "invalid unary token type");
  case ND_SQRT:
  case ND_EXP:
  case ND_LN:
  case ND_SIN:
  case ND_COS:
  case ND_TAN:
    break;
  }
  this->type = type;
  this->lhs = val;
}

Node::Node(node_t type, Node *lhs, Node *rhs) {
  switch(type) {
  default:
    assert(0 && "invalid binary token type");
  case ND_ADD:
  case ND_SUB:
  case ND_MUL:
  case ND_DIV:
  case ND_POW:
    break;
  }
  this->type = type;
  this->lhs = lhs;
  this->rhs = rhs;
}

Node::~Node() {
  if (lhs) delete lhs;
  if (rhs) delete rhs;
}

double Node::eval(VariablePayload vars) {
  switch(type) {
  default:
    assert(0 && "invalid token type");
  case ND_CONST:
    return value;
  case ND_VAR:
    return vars.get(name);
  case ND_ADD:
    return lhs->eval(vars) + rhs->eval(vars);
  case ND_SUB:
    return lhs->eval(vars) - rhs->eval(vars);
  case ND_MUL:
    return lhs->eval(vars) * rhs->eval(vars);
  case ND_DIV:
    return lhs->eval(vars) / rhs->eval(vars);
  case ND_SQRT:
    return std::sqrt(lhs->eval(vars));
  case ND_POW:
    return std::pow(lhs->eval(vars), rhs->eval(vars));
  case ND_EXP:
    return std::exp(lhs->eval(vars));
  case ND_LN:
    return std::log(lhs->eval(vars));
  case ND_SIN:
    return std::sin(lhs->eval(vars));
  case ND_COS:
    return std::cos(lhs->eval(vars));
  case ND_TAN:
    return std::tan(lhs->eval(vars));
  };
}

// Pulic methods to ease of use
double Node::eval() {
  return eval(VariablePayload());
}
double Node::eval(double var) {
  return eval(VariablePayload(var));
}
double Node::eval(const std::map<std::string, double> &vars) {
  return eval(VariablePayload(&vars));
}

// =========================================== TOKEN

Tokenizer::Tokenizer(const string &s) :
  eq(s), start(0), cur(0), max(s.length()), curTy(tok_invalid) {
}

static bool oneOf(const char a, std::initializer_list<const char> list) {
  for (auto c : list)
    if (a == c)
      return true;
  return false;
}

char Tokenizer::peek(unsigned step) const {
  return eq[cur+step];
}

// Core of the tokenizer
void Tokenizer::parse() {
  start = cur;
  bool isNum=false, isName=false;
  curTy = tok_invalid;

  if (start == max) {
    curTy = tok_end;
    return;
  }

  // Ignore spaces
  while (eq[start] == ' ')
    start++;

  // Parse token, set type
  for (cur=start; cur<max; cur++) {
    // Numbers in progress
    if (isNum && (isdigit(eq[cur]) || oneOf(eq[cur], {'e', '.', '-'}))) {
      continue;
    }
    // Names in progress
    if (isName && (isalpha(eq[cur]) || isdigit(eq[cur]) ||
                   oneOf(eq[cur], {'_'}))) {
      continue;
    }
    // boundary reached, stop, type should be set
    if (isName || isNum)
      return;

    // New var/call
    if (isalpha(eq[cur])) {
      isName=true;
      curTy = tok_name;
      continue;
    }
    // New number
    if (isdigit(eq[cur])) {
      isNum=true;
      curTy = tok_const;
      continue;
    }
    // New negative number
    // FIXME: Won't catch "- 3"
    if (eq[cur] == '-' && isdigit(peek(1))) {
      isNum=true;
      curTy = tok_const;
      continue;
    }
    // Single-char matches
    if (eq[cur] == '(') {
      curTy = tok_subexp;
      cur++;
      return;
    }
    if (eq[cur] == ')') {
      curTy = tok_closexp;
      cur++;
      return;
    }
    if (oneOf(eq[cur], {'+', '-', '*', '/', '^'})) {
      curTy = tok_op;
      cur++;
      return;
    }

    // Hum...
    assert(0 && "invalid char in token expression");
  }
}

Tokenizer::TokTy Tokenizer::type() const {
  return curTy;
}

double Tokenizer::value() const {
  assert(curTy == tok_const);
  return stod(eq.substr(start, cur-start));
}

string Tokenizer::name() const {
  assert(curTy == tok_name);
  return eq.substr(start, cur-start);
}

char Tokenizer::op() const {
  assert(curTy == tok_op);
  return eq[start];
}

// =========================================== PARSER

EquationParser::EquationParser(string s) :
  equation(s), parsed(false), Tok(equation) { }

unsigned EquationParser::numVars() const {
  return variables.size();
}

double EquationParser::eval() {
  if (!parsed)
    parse();
  assert(variables.size() == 0);
  return root->eval();
}

double EquationParser::eval(double var) {
  if (!parsed)
    parse();
  assert(variables.size() == 1);
  return root->eval(var);
}

double EquationParser::eval(const map<string, double> &vals) {
  if (!parsed)
    parse();
  assert(variables.size() == vals.size());
  return root->eval(vals);
}

bool EquationParser::parse() {
  root = parseNode();
  parsed = true;
  return parsed;
}

Node *EquationParser::parseNode() {
  // Binary nodes have left hand side first
  Tok.parse();
  Node *LHS = nullptr;
  switch(Tok.type()) {
  default:
    assert(0 && "invalid token type");
  case Tokenizer::tok_closexp:
  case Tokenizer::tok_end:
    return nullptr;
  case Tokenizer::tok_subexp:
    LHS = parseNode();
    break;
  case Tokenizer::tok_name:
    // If name is recognised as a call
    if (Node *n = parseCall())
      LHS = n;
    // If not, it's a variable
    else
      LHS = parseVariable();
    break;
  case Tokenizer::tok_const:
    LHS = parseConstant();
    break;
  case Tokenizer::tok_op:
    assert(0 && "unary basic ops not supported yet");
  }

  // If LHS is unary, return
  // Otherwise, make sure next symbol is an operator
  Tok.parse();
  switch(Tok.type()) {
  default:
    assert(0 && "invalid token after expression");
  case Tokenizer::tok_closexp:
  case Tokenizer::tok_end:
    return LHS;
  case Tokenizer::tok_op:
    break;
  }

  // Parse operator
  char oper = Tok.op();
  Node::node_t op = Node::ND_INVALID;
  if (oper == '+')
    op = Node::ND_ADD;
  else if (oper == '-')
    op = Node::ND_SUB;
  else if (oper == '*')
    op = Node::ND_MUL;
  else if (oper == '/')
    op = Node::ND_DIV;
  else if (oper == '^')
    op = Node::ND_POW;
  else
    assert(0 && "invalid binary op");

  // Parse right hand side
  Node *RHS = parseNode();
  return new Node(op, LHS, RHS);
}

Node *EquationParser::parseConstant() {
  return new Node(Tok.value());
}

Node *EquationParser::parseVariable() {
  variables.insert(Tok.name());
  return new Node(Tok.name());
}

Node *EquationParser::parseCall() {
  string func = Tok.name();
  Node::node_t op = Node::ND_INVALID;
  if (func == "sqrt")
    op = Node::ND_SQRT;
  else if (func == "exp")
    op = Node::ND_EXP;
  else if (func == "ln")
    op = Node::ND_LN;
  else if (func == "sin")
    op = Node::ND_SIN;
  else if (func == "cos")
    op = Node::ND_COS;
  else if (func == "tan")
    op = Node::ND_TAN;
  else
    return nullptr;

  assert(Tok.peek(0) == '(' && "function call without parenthesis");

  // Parse internal expression
  Node *LHS = parseNode();

  return new Node(op, LHS);
}
