#ifndef EQUATIONPARSER_H_
#define EQUATIONPARSER_H_
#include <cassert>
#include <string>
#include <map>
#include <set>
#include <array>

namespace numcalc {

using MultiVarTy = std::map<std::string, double>;

class VariablePayload {
  enum VarTy {
    None, One, Many
  } ty;
  const double Single;
  const MultiVarTy *Multi;

public:
  VariablePayload() :
    ty(None), Single(0.0), Multi(nullptr) {}
  VariablePayload(double d) :
    ty(One), Single(d), Multi(nullptr) {}
  VariablePayload(const MultiVarTy *m) :
    ty(Many), Single(0.0), Multi(m) {}

  // Get variable value
  double get(const std::string &name);
};

class Node {
public:
  enum node_t {
    ND_CONST,  // ex: 3.1415
    ND_VAR,    // ex: x
    // ================= BINARY OPS
    ND_ADD,    // ex: x + 2
    ND_SUB,    // ex: 4 - 1
    ND_MUL,    // ex: 1 * y
    ND_DIV,    // ex: 1 / y
    ND_POW,    // ex: x ^ 2
    // ================= UNARY OPS
    ND_SQRT,   // ex: sqrt(x)
    ND_EXP,    // ex: exp(x)
    ND_LN,     // ex: log(x)
    ND_SIN,    // ex: sin(x + M_PI)
    ND_COS,    // ex: cos(x + M_PI)
    ND_TAN,    // ex: tan(x + M_PI)
    // ================= END
    ND_INVALID
    // TODO:
    //  * nodes for all stdlib functions
    //  * constant fold ops on constants
    //  * sub-expr elim
    //  * strength reduce
    //  * derivates
  };

private:
  // Type is compulsory
  node_t type;
  // Constant value
  double value;
  // Variable name
  std::string name;
  // Unary value or Binary left side
  Node *lhs;
  // Binary right side
  Node *rhs;

  double eval(VariablePayload vars);
public:
  // Constuct a constant
  Node(double);
  // Constuct a variable
  Node(std::string);
  // Constuct an unary op
  Node(node_t, Node *val);
  // Constuct a binary op
  Node(node_t, Node *lhs, Node* rhs);
  // Destructor, deletes children first
  ~Node();

  node_t getType() const { return type; }
  // Evaluate all lhs and rhs tied to it, then this, and return
  double eval();
  double eval(double var);
  double eval(const MultiVarTy &vars);
};

class Tokenizer {
public:
  // Basic token logic, splits by spaces
  enum TokTy {
    tok_invalid, // everything else (failure)
    tok_name,    // names
    tok_const,   // numbers
    tok_op,      // arithmetic
    tok_subexp,  // sub-expression (parenthesis)
    tok_closexp, // end-expression (parenthesis)
    tok_end      // end of the equation
  };

private:
  unsigned curPos;
  const std::string &eq; // no ownership

  unsigned start;
  unsigned cur;
  const unsigned max;
  TokTy curTy;
public:
  Tokenizer(const std::string &s);
  void parse();
  TokTy type() const;
  double value() const;
  std::string name() const;
  char op() const;
  void consume(std::string text) const;
  char peek(unsigned step) const;
};

class EquationParser {
  // Root node, contains all remaining nodes recursively
  Node *root;
  // Cached equation
  std::string equation;
  // Flag to make sure it's parsed before eval
  bool parsed;
  // Tokenizer
  Tokenizer Tok;
  // Number of variables expected
  std::set<std::string> variables;

  // Recursive node parser call
  Node *parseNode();
  // Specific parser routines acting on cached equation
  Node *parseConstant();
  Node *parseVariable();
  Node *parseCall();
public:
  // Constructs an equation with its text. Identifiers not recognised
  // will be considered variables.
  EquationParser(std::string);
  ~EquationParser() { delete root; }
  // Separate parsing function to allow failure recovery
  bool parse();
  // Return the number of variables it's expecting
  unsigned numVars() const;
  // Return the value of a constant equation (no variables)
  double eval();
  // Return the value of an equation with just one variable
  double eval(double var);
  // Return the value of the equation with a set of values for the variables
  double eval(const MultiVarTy &vars);
  // Cached equation
  const std::string &getEquation() const { return equation; }
};

} // namespace numcalc

#endif // EQUATIONPARSER_H_
