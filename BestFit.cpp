#include <cassert>
#include <sstream>
#include "BestFit.h"

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

// Least Squares
LeastSquares::LeastSquares(const matrix_t &A, const vector_t &b) :
  A(A), b(b), error(0.0) {
  matrix_t AT = A.transpose();
  DEBUG("    Transposed A: " << std::endl << AT);
  matrix_t NA = AT*A;
  DEBUG("    Transposed A * A: " << std::endl << NA);
  vector_t nb = AT*b;
  DEBUG("    Transposed A * b: " << nb);
  // Concat NA and nb together and solve the matrix
  // FIXME: Add this to math.h
  matrix_t M(NA.rows, NA.cols+1);
  for (unsigned i=0; i<NA.rows; i++)
    for (unsigned j=0; j<NA.cols; j++)
      M(i, j) = NA[i][j];
  for (unsigned i=0; i<M.rows; i++)
    M(i, NA.cols) = nb[i];
  DEBUG("    Matrix to solve: " << std::endl << M);
  // Create the solver
  MS = new MatrixSolver(M);
}
LeastSquares::~LeastSquares() {
  delete MS;
}
double LeastSquares::getError() {
  if (error != 0.0)
    return error;

  // x'
  vector_t x(A.cols);
  for (unsigned i=0; i<A.cols; i++)
    x(i) = MS->getVar(i);
  DEBUG("    Coefs: " << x);

  // Ax'
  vector_t Ax = A*x;
  DEBUG("    A * Coefs: " << Ax);
  // r = b - Ax'
  vector_t e = b - Ax;
  DEBUG("    b - A * Coefs: " << e);

  // RMS Error
  error = 0.0;
  for (unsigned i=0; i<A.rows; i++)
    error += e[i]*e[i];
  DEBUG("    |Error|: " << error);
  error /= A.rows;
  DEBUG("    |Error|/m: " << error);
  error = sqrt(error);
  DEBUG("    RMS Error: " << error);
  return error;
}

// Poly Fit
PolyFit::PolyFit(const points_t &P, unsigned degree) :
                 Fit(degree+1), degree(degree) {
  DEBUG("  PolyFit[" << degree << "]");
  matrix_t A(P.size(), degree+1);
  vector_t b(P.size());
  for (unsigned i=0; i<P.size(); i++) {
    double power = P[i].x;
    A(i, 0) = 1;
    A(i, 1) = power;
    for (unsigned n=2; n<=degree; n++) {
      power *= P[i].x;
      A(i, n) = power;
    }
    b(i) = P[i].y;
  }
  DEBUG("    A: " << std::endl << A);
  DEBUG("    b: " << b);
  LS = new LeastSquares(A, b);
}

// Periodic Fit
PeriodicFit::PeriodicFit(const points_t &P, unsigned degree) :
                         Fit(2*degree+1), limits(P), degree(degree) {
  DEBUG("  PeriodicFit[" << degree << "]");
  DEBUG("    Range = [" << limits.start << ", " << limits.stop << "]");
  DEBUG("    Length = [" << limits.length() << "]");
  matrix_t A(P.size(), 2*degree+1);
  vector_t b(P.size());
  for (unsigned i=0; i<P.size(); i++) {
    A(i, 0) = 1;
    for (unsigned n=1; n<=degree; n++) {
      double teta = 2*M_PI*n;
      DEBUG("    TETA = [" << teta << "]");
      double range = P[i].x-P[0].x;
      DEBUG("    Range = [" << range << "]");
      double frac = range/limits.length();
      DEBUG("    Frac = [" << frac << "]");
      A(i, 2*n-1) = cos(teta*frac);
      A(i, 2*n)   = sin(teta*frac);
    }
    b(i) = P[i].y;
  }
  DEBUG("    A: " << std::endl << A);
  DEBUG("    b: " << b);
  LS = new LeastSquares(A, b);
}

// Logarithm Fit
#ifdef _DEBUG
static const char *LogFitTypeName(LogFitType type) {
  switch(type) {
  case LogFitType::EXP:
    return "exponential";
  case LogFitType::PWR:
    return "power";
  case LogFitType::HL:
    return "half-life";
  default:
    return "";
  }
}
#endif
LogarithmFit::LogarithmFit(const points_t &P, LogFitType type) :
                           Fit(2), type(type) {
  DEBUG("  LogarithmFit: " << LogFitTypeName(type));
  matrix_t A(P.size(), 2);
  vector_t b(P.size());
  for (unsigned i=0; i<P.size(); i++) {
    A(i, 0) = 1;
    DEBUG("    Point: " << P[i]);
    switch(type) {
    case LogFitType::EXP:
      A(i, 1) = P[i].x;
      b(i) = log(P[i].y);
      break;
    case LogFitType::PWR:
      A(i, 1) = log(P[i].x);
      b(i) = log(P[i].y);
      break;
    case LogFitType::HL:
      A(i, 1) = P[i].x;
      b(i) = log(P[i].y) - log(P[i].x);
      break;
    default:
      assert(0 && "Unknwon log fit type");
    }
  }
  DEBUG("    A: " << std::endl << A);
  DEBUG("    b: " << b);
  LS = new LeastSquares(A, b);
}

// Best Fit
BestFit::BestFit(const points_t &P) : best(nullptr) {
  DEBUG("  BestFit");
  DEBUG("  Adding polinomial fitters");
  for (unsigned i=0; i<PolyMax; i++) {
    fits.push_back(new PolyFit(P, i+1));
  }
  // Only allow periodic if cyclic
  if (isZero(P[0].y-P[P.size()-1].y)) {
    DEBUG("  Adding periodical fitters");
    for (unsigned i=0; i<PeriMax; i++) {
      fits.push_back(new PeriodicFit(P, i+1));
    }
  }
  // Only try log fitters if x,y > 0
  for (auto p : P)
    if (p.x < 0 || p.y < 0)
      return;
  DEBUG("  Adding logarithmic fitters");
  fits.push_back(new LogarithmFit(P, LogFitType::EXP));
  fits.push_back(new LogarithmFit(P, LogFitType::PWR));
  fits.push_back(new LogarithmFit(P, LogFitType::HL));
}
BestFit::~BestFit() {
  for (auto fit : fits)
    delete fit;
}
Fit *BestFit::getBest() {
  if (best != nullptr)
    return best;

  double min = 1e10;
  for (unsigned i=0, max=fits.size(); i<max; i++) {
    double error = fits[i]->getError();
    DEBUG("  Fit[" << i+1 << "], error = " << error);
    if (error < min) {
      best = fits[i];
      min = error;
    }
  }
  DEBUG("  Minimum error = " << min);
  return best;
}

// Helper to print the equation in text format. It can be used to validate
// the solver and also as a gnuplot output.
std::string PrintEquation(Fit *f) {
  std::stringstream ss;

  if (PolyFit *pf = dynamic_cast<PolyFit *>(f)) {
    ss << pf->getCoef(0);
    for (unsigned i=1, max=pf->getDegree(); i<=max; i++) {
      double c = pf->getCoef(i);
      if (c == 0.0 || c == -0.0)
        continue;
      if (c >= 0.0)
        ss << "+" << c << "*x**" << i;
      else
        ss << "-" << std::abs(c) << "*x**" << i;
    }
    return ss.str();

  } else if (PeriodicFit *pf = dynamic_cast<PeriodicFit *>(f)) {
    ss << pf->getCoef(0);
    for (unsigned i=1, max=pf->getDegree(); i<=max; i++) {
      bool sin = false;
      for (int j=1; j>=0; j--) {
        double c = pf->getCoef(2*i-j);
        if (c == 0.0 || c == -0.0)
          continue;
        if (c >= 0.0)
          ss << "+" << c << "*";
        else
          ss << "-" << std::abs(c) << "*";
        ss << (sin ? "sin(" : "cos(") << 2*i << "*pi*x)";
        sin = true;
      }
    }
    return ss.str();

  } else if (LogarithmFit *lf = dynamic_cast<LogarithmFit *>(f)) {
    switch(lf->getType()) {
    case LogFitType::EXP:
      ss << exp(lf->getCoef(0)) << "*exp(" << lf->getCoef(1) << "*x)";
      return ss.str();
    case LogFitType::PWR:
      ss << exp(lf->getCoef(0)) << "*x**" << lf->getCoef(1);
      return ss.str();
    case LogFitType::HL:
      ss << exp(lf->getCoef(0)) << "*x*exp(" << lf->getCoef(1) << "*x)";
      return ss.str();
    }
  }
  return "ERROR";
}

} // namespace numcalc
