#include <iostream>
#include <vector>
#include "EquationParser.h"
#include "Derivate.h"

using namespace std;
using namespace numcalc;

// Usage: cat derivate.txt | derivate
// Returns derivative in points Xn
int main() {
  string eq;
  getline(cin, eq);
  EquationParser parser(eq);
  if (!parser.parse()) {
    cerr << "ERROR: Cannot parse equation" << endl;
    cerr << " -> '" << eq << "'" << endl;
  }

  unsigned points;
  cin >> points;
  double value;
  for (unsigned i=0; i<points; i++) {
    cin >> value;

    function<double (double)> eval =
         [&](double x) { return parser.eval(x); };
    Derivate d(eval);
    cout << " f'(" << value << ") = " <<  d(value) << endl;

    SecondDerivate sd(eval);
    cout << "f''(" << value << ") = " << sd(value) << endl;
    cout << endl;
  }

  return 0;
}
