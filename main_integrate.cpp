#include <iostream>
#include <fstream>
#include "Integrate.h"
#include "Poly.h"

using namespace std;
using namespace numcalc;

// Capture lambdas don't cast to function pointers
// We need a global variable. FIXME: Make Poly better.
Poly *poly;

// Usage: cat equation.txt | integrate
// Returns function integral from A to B
int main () {
  unsigned degree;
  double start, stop;
  cin >> degree >> start >> stop;
  if (degree > 10 || start > stop) {
    cerr << "ERROR: Equation file format:" << endl;
    cerr << "degree from to (int double double)" << endl;
    cerr << "c1 c2 c3 ... c(degree) (double coefs)" << endl;
    return 1;
  }

  vector<double> coefs;
  double value;
  for (unsigned i=0; i<degree; i++) {
    cin >> value;
    coefs.push_back(value);
  }
  poly = new Poly(coefs);

  // compile with c++11 support
  Integrate it([](double x) { return (*poly)(x); });
  try {
    cout << "Integral from (" << start << ") to (" << stop << ") is: " << it.sum(start, stop) << endl;
  } catch (...) {
    cerr << "ERROR: Exception on integral" << endl;
    return 1;
  }

  return 0;
}
