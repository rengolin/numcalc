#ifndef ODEH
#define ODEH
#include "Math.h"

namespace numcalc {

/*
 * Runge-Kutta Ordinary differential equation solver (k4)
 *
 * Ch.8, Elementary Differential Equations and Boundary Value Problems
 * Boyce, Di Prima
 */
class ODE {
  double (*f)(point_t);
  double step;
public:
  ODE(double (*f)(point_t), double step) : f(f), step(step) {}
  point_t next(point_t t0, unsigned steps);
};

class ODE2 {
  double (*fx)(point_t);
  double (*fy)(point_t);
  double step;
public:
  ODE2(double (*fx)(point_t), double (*fy)(point_t), double step) : fx(fx), fy(fy), step(step) {}
  point_t next(point_t t0, unsigned steps);
};

} // namespace numcalc

#endif // ODEH
