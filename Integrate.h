#ifndef INTEGRATE_H_
#define INTEGRATE_H_

namespace numcalc {

/*
 * This class implements the fast Simpson's rule for
 * integration. It progressively divide the chunk size and updates
 * only the delta relative to the previous calculation, until the
 * required precision is achieved.
 *
 * Ch.5, Numerical Analysis, T. Sauer, Pearson
 */
class Integrate {
	double (*f)(double);
	unsigned depth;
	double error;
public:
	// Set functor and depth
	Integrate(double (*f)(double), unsigned d=1024) : f(f), depth(d) { }
	~Integrate() { };
	// Integrate
	double sum (double, double);
};

} // namespace numcalc

#endif /*INTEGRATE_H_*/
