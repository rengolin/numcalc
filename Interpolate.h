#ifndef INTERPOLATE_H_
#define INTERPOLATE_H_
#include "Math.h"

namespace numcalc {

/* 
 * Natural spline (y"[0] = y"[L] = 0)
 *
 * Ch.11, Computer Graphics using OpenGL, F.S. Hill, JR.
 */

// Cubic spline coefficients
struct coef_t {
  point_t a;
  point_t b;
  point_t c;
  point_t d;
};
typedef std::vector<coef_t> coefs_t;

// Main class
class Interpolate {
  points_t points;
  unsigned size;
  coefs_t c;

  void calculateCoefs();

public:
  Interpolate(const points_t &P);

  // Get the interpolated values via cubic interpolation
  point_t getPoint(double t);
  
  // Get cubic coefs for a certain region
  coef_t getCoefs(unsigned interval);
};

} // namespace numcalc

#endif /*INTERPOLATE_H_*/
