# FLAGS
DEFAULT_FLAGS = -Wall -pedantic -std=c++11 -Wno-deprecated
LDFLAGS = -lm
ifeq ($(DEBUG),1)
CPPFLAGS = -Og -g $(DEFAULT_FLAGS) -D_DEBUG
else
CPPFLAGS = -O2 $(DEFAULT_FLAGS)
endif
ifeq ($(LEAKSAN),1)
CPPFLAGS += -fsanitize=leak
LDFLAGS += -llsan
endif
ifeq ($(ADDSAN),1)
CPPFLAGS += -fsanitize=address
LDFLAGS += -lasan
endif
.PHONY: check

# Tools
MAIN := $(wildcard main_*.cpp)
MAIN_OBJS := $(MAIN:%.cpp=%.o)
TOOLS := $(MAIN:main_%.cpp=%)

# General objects
SRCS := $(wildcard *.cpp)
SRCS := $(filter-out $(MAIN),$(SRCS))
OBJS := $(SRCS:%.cpp=%.o)

# All + unit-tests
all: tools check
	@echo Done

poly: main_poly.cpp
	$(CXX) -o $@ $(LDFLAGS) $^

matrix: main_matrix.o Math.o
	$(CXX) -o $@ $(LDFLAGS) $^

root: main_root.o RootFinder.o Math.o
	$(CXX) -o $@ $(LDFLAGS) $^

integrate: main_integrate.o Integrate.o Math.o
	$(CXX) -o $@ $(LDFLAGS) $^

interpolate: main_interpolate.o Interpolate.o Math.o
	$(CXX) -o $@ $(LDFLAGS) $^

ode: main_ode.o ODE.o
	$(CXX) -o $@ $(LDFLAGS) $^

chain: main_chain.o
	$(CXX) -o $@ $(LDFLAGS) $^

fit: main_fit.o BestFit.o Math.o
	$(CXX) -o $@ $(LDFLAGS) $^

derivate: main_derivate.o EquationParser.o Math.o
	$(CXX) -o $@ $(LDFLAGS) $^

equation: main_equation.o EquationParser.o
	$(CXX) -o $@ $(LDFLAGS) $^

tools: $(TOOLS)

# Tests
TSTS := $(wildcard tests/test_*.cpp)
TSTS_OBJS := $(TSTS:%.cpp=%.o)
TESTS := $(TSTS:%.cpp=%)

tests/test_%: tests/test_%.o $(OBJS)
	$(CXX) -o $@ $(LDFLAGS) $^

check: $(TESTS)
	@for test in $^; do ./$$test; done

clean:
	rm -f $(OBJS) $(MAIN_OBJS) $(TSTS_OBJS)
	rm -f $(TESTS) $(TOOLS)
	rm -f core* gmon.out *.gp *.out
