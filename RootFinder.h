#ifndef ROOTFINDER_H_
#define ROOTFINDER_H_

namespace numcalc {

/*
 * This class uses three different approaches to root finding:
 *
 * 1. Bisect find: binary search of values inverting sign. If the
 * values invert signs initially, this is guaranteed to find the answer,
 * but it can take a very long time.
 *
 * 2. Secant method: A variation of Newton's method with slightly less
 * worse cases. Basically finds the secant between the two points and solve
 * that line equation until the root is found. Faster than bisection.
 *
 * 3. Brent's method: Similar to secant, but approximating with a
 * quadractic curve. Significantly faster than the other two and the default
 * implementation in "find".
 *
 * Ch.1, Numerical Recipes, T. Sauer, Pearson
 */
class RootFinder {
  double (*f)(double);
  unsigned depth;
  unsigned steps;

  // Finds an appropriate bracket to start the methods. This may fail on
  // cyclic functions or high power polynomials.
  // TODO: Add random numbers (up to 10%) to the brackets.
  bool findBracket(double *a, double *b);
public:
  RootFinder(double (*f)(double)) : f (f), depth (100), steps(0) { }
  ~RootFinder() { };

  unsigned getSteps() { return steps; }

  // Brent's Method
  double find(double);

  // Simplified Newton-Raphson's method
  double secantFind(double);

  // Basic bisection method
  double bisectFind(double);

  // TODO: Produce a hybrid that tries all three methods in sequence and
  // stop when the backward error is low enough or at least the bracket has
  // reduced to less than 1/2 of the previous size.
};

} // namespace numcalc

#endif /*ROOTFINDER_H_*/
