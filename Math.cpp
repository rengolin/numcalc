#include <iomanip>
#include "Math.h"

namespace numcalc {

// ===================================== TYPES

// 2D Point
point_t& point_t::operator=(const point_t &o) {
  x = o.x;
  y = o.y;
  return *this;
}
bool point_t::operator==(const point_t &o) const {
  return x == o.x && y == o.y;
}
bool point_t::operator!=(const point_t &o) const {
  return x != o.x || y != o.y;
}
point_t point_t::operator+(const point_t &o) const {
  return point_t(x+o.x, y+o.y);
}
point_t point_t::operator-(const point_t &o) const {
  return point_t(x-o.x, y-o.y);
}
point_t point_t::operator-() const {
  return point_t(-x, -y);
}
point_t point_t::operator*(int a) const {
  return point_t(x*a, y*a);
}
std::ostream &operator<< (std::ostream &out, const point_t &p) {
  out << "(" << p.x << ", " << p.y << ")";
  return out;
}

// Vector
vector_t& vector_t::operator=(const vector_t &o) {
  new (this) vector_t(o);
  return *this;
}
double vector_t::operator[](unsigned i) const {
  assert(i < data.size());
  return data[i];
}
double& vector_t::operator()(unsigned i) {
  assert(i < data.size());
  return data[i];
}
bool vector_t::operator==(const vector_t &o) const {
  if (data.size() != o.data.size())
    return false;
  for (unsigned i=0, N=data.size(); i<N; i++)
    if (data[i] != o[i])
      return false;
  return true;
}
bool vector_t::operator!=(const vector_t &o) const {
  if (data.size() != o.data.size())
    return true;
  for (unsigned i=0, N=data.size(); i<N; i++)
    if (data[i] != o[i])
      return true;
  return false;
}
vector_t vector_t::operator+(const vector_t&o) const {
  assert(data.size() == o.data.size());
  vector_t out(o.data.size());
  for (unsigned i=0, N=data.size(); i<N; i++)
    out(i) = data[i] + o[i];
  return out;
}
vector_t vector_t::operator-(const vector_t&o) const {
  assert(data.size() == o.data.size());
  vector_t out(o.data.size());
  for (unsigned i=0, N=data.size(); i<N; i++)
    out(i) = data[i] - o[i];
  return out;
}
vector_t vector_t::operator-() const {
  vector_t out(data.size());
  for (unsigned i=0, N=data.size(); i<N; i++)
    out(i) = -data[i];
  return out;
}
vector_t vector_t::operator*(const int a) const {
  vector_t out(data.size());
  for (unsigned i=0, N=data.size(); i<N; i++)
    out(i) = data[i]*a;
  return out;
}
std::ostream &operator<< (std::ostream &out, const vector_t &v) {
  out << "(" << v.data[0];
  for (unsigned i=1, N=v.data.size(); i<N; i++)
    std::cout << ", " << v.data[i];
  std::cout << ")";
  return out;
}

// Matrix
matrix_t::matrix_t(unsigned rows, unsigned cols) :
  rows(rows), cols(cols) {
  data.resize(rows);
  for (unsigned i=0; i<rows; ++i)
    data[i] = new double[cols]();
}
matrix_t::matrix_t(const matrix_t &m) :
  rows(m.rows), cols(m.cols) {
  data.resize(rows);
  for (unsigned i=0; i<rows; ++i) {
    data[i] = new double[cols];
    for (unsigned j=0; j<cols; j++)
      data[i][j] = m.data[i][j];
  }
}
matrix_t::matrix_t(const std::initializer_list<std::initializer_list<double>>& list) : rows(list.size()), cols(list.begin()->size()) {
  unsigned i=0, j=0;
  data.resize(rows);
  for (const std::initializer_list<double> &l : list) {
    data[i] = new double[cols];
    j=0;
    for (const double &d : l)
      data[i][j++] = d;
    i++;
  }
}
matrix_t::~matrix_t() {
  for (double* d: data)
    delete [] d;
}
matrix_t& matrix_t::operator=(const matrix_t &o) {
  new (this) matrix_t(o);
  return *this;
}
const double* matrix_t::operator[] (unsigned i) const {
  assert(i < rows);
  return data[i];
}
double& matrix_t::operator() (unsigned r, unsigned c) {
  assert(r < rows || c < cols);
  return data[r][c];
}
void matrix_t::swap(unsigned r1, unsigned r2) {
  assert (r1 < rows || r2 < rows);
  if (r1 == r2)
    return;
  std::swap(data[r1], data[r2]);
}
matrix_t matrix_t::transpose() const {
  matrix_t out(cols, rows);
  for (unsigned i=0; i<cols; i++)
    for (unsigned j=0; j<rows; j++)
      out(i, j) = data[j][i];
  return out;
}
bool matrix_t::operator==(const matrix_t &o) const {
  if (o.rows != rows || o.cols != cols)
    return false;
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      if (data[i][j] != o.data[i][j])
        return false;
  return true;
}
bool matrix_t::operator!=(const matrix_t &o) const {
  if (o.rows != rows || o.cols != cols)
    return true;
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      if (data[i][j] != o.data[i][j])
        return true;
  return false;
}
matrix_t matrix_t::operator+(const matrix_t &o) const {
  assert(o.rows == rows && o.cols == cols);
  matrix_t out(rows, cols);
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      out(i, j) = data[i][j] + o.data[i][j];
  return out;
}
matrix_t matrix_t::operator-(const matrix_t &o) const {
  assert(o.rows == rows && o.cols == cols);
  matrix_t out(rows, cols);
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      out(i, j) = data[i][j] - o.data[i][j];
  return out;
}
matrix_t matrix_t::operator*(const matrix_t &o) const {
  assert(o.rows == cols && o.cols == rows);
  assert(rows <= cols && "wrong multiplication order");
  matrix_t out(rows, rows);
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<rows; j++)
      for (unsigned k=0; k<cols; k++)
        out(i, j) += data[i][k] * o.data[k][j];
  return out;
}
vector_t matrix_t::operator*(const vector_t &o) const {
  assert(o.len == cols);
  vector_t out(rows);
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      out(i) += data[i][j] * o[j];
  return out;
}
matrix_t matrix_t::operator*(const int a) const {
  matrix_t out(rows, cols);
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      out(i, j) = data[i][j] * a;
  return out;
}
matrix_t matrix_t::operator-() const {
  matrix_t out(rows, cols);
  for (unsigned i=0; i<rows; i++)
    for (unsigned j=0; j<cols; j++)
      out(i, j) = -data[i][j];
  return out;
}
std::ostream &operator<< (std::ostream &out, const matrix_t &m) {
  for (unsigned i=0, N=m.rows; i<N; i++) {
    out << "|";
    if (m[i][0] >= 0)
      out << " ";
    out << std::fixed << std::setprecision(2) << m[i][0];
    for (unsigned j=1, M=m.cols; j<M; j++) {
      if (m[i][j] >= 0)
        out << " ";
      out << std::fixed << std::setprecision(2) << " " << m[i][j];
    }
    out << " |" << std::endl;
  }
  return out;
}

// Range
range_t::range_t(const points_t &P, bool YAxis) {
  start = stop = (YAxis ? P[0].y : P[0].x);
  for (auto p: P) {
    start = std::min(start, (YAxis ? p.y : p.x));
    stop = std::max(stop, (YAxis ? p.y : p.x));
  }
}
range_t::range_t(const vector_t &V) {
  start = stop = V[0];
  for (unsigned i=1; i<V.len; i++) {
    start = std::min(start, V[i]);
    stop = std::max(stop, V[i]);
  }
}


// ===================================== UTILITIES

// Test for zero within defined quality (positive delta)
bool isZero(double x, double small) {
	return (small > std::abs(x));
}

// Test if values have inverted signals
bool inverted(double x, double y) {
	return (x * y < 0);
}

} // namespace numcalc
