#include <cassert>
#include "Interpolate.h"

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

Interpolate::Interpolate(const points_t &P) : size(P.size()), c(size) {
  assert(size >= 3 && "Can't interpolate with less than 3 points");
  DEBUG("\tInterpolate " << size << " points");
  for (auto point : P) {
    DEBUG("\t\tAdding point: " << point);
    points.push_back(point);
  }
  calculateCoefs();
}

void
Interpolate::calculateCoefs() {
  points_t v(size); // sub-diagonal
  points_t q(size); // distances between points
  points_t s(size); // first derivs

  // First, we solve the derivatives matrix
  v[0].x = v[size-1].x = 1/4;
  v[0].y = v[size-1].y = 1/4;
  q[0].x = 3.0 * (points[1].x-points[0].x)*v[0].x;
  q[0].y = 3.0 * (points[1].y-points[0].y)*v[0].y;
  DEBUG("\t\t\tSub-diag[0] = sub-diag[size] = " << v[0]);
  DEBUG("\t\t\t    Dist[0] = " << q[0]);
  for (unsigned i=1; i<size; i++) {
    v[i].x = 1.0 / (4.0-v[i-1].x);
    v[i].y = 1.0 / (4.0-v[i-1].y);
    q[i].x = (3.0 * (points[i].x-points[i-1].x) - q[i-1].x) * v[i].x;
    q[i].y = (3.0 * (points[i].y-points[i-1].y) - q[i-1].y) * v[i].y;
    DEBUG("\t\t\tSub-diag[" << i << "] = " << v[i]);
    DEBUG("\t\t\t    Dist[" << i << "] = " << q[i]);
  }

  // Now, we calculate the derivatives
  s[size-1].x = q[size-1].x;
  s[size-1].y = q[size-1].y;
  for (unsigned i=size-2; i>0; i--) {
    s[i].x = q[i].x - v[i].x * s[i+1].x;
    s[i].y = q[i].y - v[i].y * s[i+1].y;
    DEBUG("\t\t\t1s-deriv[" << i << "] = " << s[i]);
  }

  // And finally, the coeficients
  for (unsigned i=0; i<size-1; i++) {
    coef_t& C = c[i];
    point_t dist (points[i+1].x-points[i].x, points[i+1].y-points[i].y);

    C.a.x = s[i+1].x + s[i].x - 2 * dist.x;
    C.a.y = s[i+1].y + s[i].y - 2 * dist.y;
    C.b.x = 3 * dist.x - 2 * s[i].x - s[i+1].x;
    C.b.y = 3 * dist.y - 2 * s[i].y - s[i+1].y;
    C.c = s[i];
    C.d = points[i];
    DEBUG("\t\t\t   Coefs[" << i << "] = " << C.a << ", " << C.b << 
                                    ", " << C.c << ", " << C.d);
  }
}

point_t
Interpolate::getPoint(double x) {
  // Short-cut for point values
  for (auto point : points) {
    if (isZero(x-point.x)) {
      DEBUG("\t\tPoint[" << x << "] = " << point);
      return point;
    }
  }

  // Find the interval
  int interval = -1;
  double t = x;
  for (unsigned i=0; i<size-1; i++) {
    if (t >= points[i].x && t < points[i+1].x) {
      interval = i;
      t -= points[i].x;
      break;
    }
  }
  assert(interval >= 0);

  // Calculate the Y from the cubic approximation
  double t2 = t*t, t3 = t2*t;
  coef_t& C = c[interval];
  point_t p;
  p.x = C.a.x*t3 + C.b.x*t2 + C.c.x*t + C.d.x;
  p.y = C.a.y*t3 + C.b.y*t2 + C.c.y*t + C.d.y;
  DEBUG("\t\tPoint[" << x << "] = " << p);

  return p;
}

coef_t
Interpolate::getCoefs(unsigned interval) {
  assert(interval < c.size());
  return c[interval];
}

} // namespace numcalc
