#include <iostream>
#include "../Chain.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Chain Test
int main() {
  unsigned error = 0;
  cout << "Markov Chain test: ";

  Chain <string> chain;
  // Markov Chain test [1 - 2,2,3,3,4 / 2 - 4 / 3 - 4]
  // No cycles, so we can guarantee number of steps
  chain.add("one", "two");
  chain.add("one", "two");
  chain.add("one", "three");
  chain.add("one", "three");
  chain.add("one", "four");
  chain.add("two", "four");
  chain.add("three", "four");

  // Following a random path based on number of connections
  unsigned steps = 0;
  string current;
  do {
    current = chain.next();
    steps++;
  } while (current != "four");
  error += LESSTHAN("Maximum number of steps", steps, 3);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
