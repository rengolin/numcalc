#include <iostream>
#include "../Math.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Maths test
int main() {
  unsigned error = 0;
  cout << "Maths test: ";

  // point_t
  point_t a(3, 4), b(-1, 3.5), z(0, 0);
  error += COMPARE("(3, 4) + (-1, 3.5)", a+b, point_t(2, 7.5));
  error += COMPARE("(-1, 3.5) + (3, 4)", b+a, point_t(2, 7.5));
  error += COMPARE("(3, 4) + (0, 0)",    a+z, a);
  error += COMPARE("(0, 0) + (-1, 3.5)", z+b, b);
  error += COMPARE("distributive and zero", a+b+z, b+z+a);

  error += COMPARE("(3, 4) - (-1, 3.5)", a-b, point_t(4, 0.5));
  error += COMPARE("(-1, 3.5) - (3, 4)", b-a, point_t(-4, -0.5));
  error += COMPARE("(3, 4) - (0, 0)",    a-z, a);
  error += COMPARE("(0, 0) - (-1, 3.5)", z-b, -b);

  error += COMPARE("(3, 4) x 5", a*5, point_t(15, 20));
  error += COMPARE("(3, 4) x -2", a*(-2), point_t(-6, -8));

  point_t zero, copy(a), assign = a;
  error += COMPARE("default ctor", zero, z);
  error += COMPARE("copy ctor", copy, a);
  error += COMPARE("assign ctor", assign, a);

  // vector_t
  vector_t va({3, 4, 5}), vb({-1, -7, -3.5}), vz({0, 0, 0});
  error += COMPARE("(3, 4, 5) - (-1, -7, -3.5)", va+vb, vector_t({2, -3, 1.5}));
  error += COMPARE("(-1, -7, -3.5) - (3, 4, 5)", vb-va, vector_t({-4, -11, -8.5}));
  error += COMPARE("(3, 4, 5) - (0, 0, 0)", va-vz, va);
  error += COMPARE("(-1, -7, -3.5) - (0, 0, 0)", vz-vb, -vb);

  error += COMPARE("(3, 4, 5) * 5", va*5, vector_t({15, 20, 25}));
  error += COMPARE("(3, 4, 5) * -2", va*(-2), vector_t({-6, -8, -10}));

  vector_t vzero(3), vcopy(va), vassign = va;
  error += COMPARE("sized ctor", vzero, vz);
  error += COMPARE("copy ctor", vcopy, va);
  error += COMPARE("assign ctor", vassign, va);

  // matrix_t
  matrix_t A({{1, 2, 3},{4, 5, 6}}), B({{-1, 3, -4},{7, 1.5, 0.1}}),
           Z({{0, 0, 0},{0, 0, 0}});
  error += COMPARE("Matrix A + B", A+B, matrix_t({{0, 5, -1},{11, 6.5, 6.1}}));
  error += COMPARE("Matrix A - B", A-B, matrix_t({{2, -1, 7},{-3, 3.5, 5.9}}));
  error += COMPARE("Matrix A + 0", A+Z, A);
  error += COMPARE("Matrix 0 - B", Z-B, -B);

  matrix_t MZERO(2, 3), MCOPY(A), MASSIGN = A;
  error += COMPARE("sized ctor", MZERO, Z);
  error += COMPARE("copy ctor", MCOPY, A);
  error += COMPARE("asign ctor", MASSIGN, A);

  matrix_t TA = A.transpose();
  error += COMPARE("Transpose A", TA, matrix_t({{1, 4},{2, 5},{3, 6}}));
  error += COMPARE("Matrix A * B", A*TA, matrix_t({{14, 32},{32, 77}}));

  error += COMPARE("Matrix * vector", A*va, vector_t({26, 62}));

  // range_t
  range_t R1(va);
  error += EXPECTED("Range length", R1.length(), 2);
  points_t PS({a, b, z});
  range_t R2X(PS);
  error += EXPECTED("Range length on X", R2X.length(), 4);
  range_t R2Y(PS, true);
  error += EXPECTED("Range length on Y", R2Y.length(), 4);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
