#include <iostream>
#include "../MatrixSolver.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Matrix solver Test
int main () {
  unsigned error = 0;
  cout << "Matrix test: ";

  {
    // Old constructor
    matrix_t M(3, 4);
    M(0, 0) =  1; M(0, 1) =  2; M(0, 2) = -1; M(0, 3) =  3;
    M(1, 0) =  2; M(1, 1) =  1; M(1, 2) = -2; M(1, 3) =  3;
    M(2, 0) = -3; M(2, 1) =  1; M(2, 2) =  1; M(2, 3) = -6;

    MatrixSolver MS(M);
    error += EXPECTED("Matrix 1 solution [z=2]", MS.getVar(2), 2.0);
    error += EXPECTED("Matrix 1 solution [y=1]", MS.getVar(1), 1.0);
    error += EXPECTED("Matrix 1 solution [x=3]", MS.getVar(0), 3.0);
  }

  {
    // initializer_list constructor
    matrix_t M({{delta, 1, 1},
                {    1, 2, 4}});

    MatrixSolver MS(M);
    error += EXPECTED("Matrix 2 solution [y=1]", MS.getVar(1), 1.0);
    error += EXPECTED("Matrix 2 solution [x=2]", MS.getVar(0), 2.0);
  }

  {
    // initializer_list constructor
    matrix_t M({{3, 1, 6},
                {1, 3, 4}});

    MatrixSolver MS(M);
    error += EXPECTED("Matrix 3 solution [y=3/4]", MS.getVar(1), 3.0/4.0);
    error += EXPECTED("Matrix 3 solution [x=7/4]", MS.getVar(0), 7.0/4.0);
  }

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
