#ifndef TEST_H_H
#define TEST_H_H
#include <iostream>
#include <iomanip>
#include "../Math.h"

static inline bool
EXPECTED(const char* text, double actual, double expected, double diff=1e-5) {
  if (!numcalc::isZero(actual - expected, diff)) {
  ;
    std::cout << std::fixed << std::setprecision(6)
              << "ERROR: " << text << ", expected [" << expected
              << "], got [" << actual << "]" << std::endl;
    return true;
  }
  return false;
}

static inline bool
LESSTHAN(const char* text, double actual, double expected, double diff=1e-5) {
  if (actual > expected - diff) {
    std::cout << std::fixed << std::setprecision(6)
              << "ERROR: " << text << ", expected less than [" << expected
              << "], got [" << actual << "]" << std::endl;
    return true;
  }
  return false;
}

static inline bool
ERROR(const char* text) {
  std::cout << "ERROR: " << text << std::endl;
  return true;
}

template <class T>
static inline bool
COMPARE(const char* text, T actual, T expected) {
  if (actual != expected) {
    std::cout << std::fixed << std::setprecision(6)
              << "ERROR: " << text << ", expected [" << expected
              << "], got [" << actual << "]" << std::endl;
    return true;
  }
  return false;
}

#endif //TEST_H_H
