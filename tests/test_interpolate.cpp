#include <iostream>
#include "../Interpolate.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Spline Test
int main () {
  unsigned error = 0;
  cout << "Interpolation test: ";

  points_t points({
    {1.0, 3.24534},
    {2.0, 7.12332},
    {3.0, 10.11196},
    {4.0, 4.23434}
  });

  Interpolate ip (points);

  // 0: A=(-1.35714 -5.59502); B=(2.35714 9.473); C=(0 0); D=(1 3.24534);
  // 1: A=(-0.928571 -0.826156); B=(1.28571 1.65386); C=(0.642857 2.16094); D=(2 7.12332);
  // 2: A=(-0.928571 9.58966); B=(1.5 -18.4575); C=(0.428571 2.99019); D=(3 10.112);
  coef_t c = ip.getCoefs(0);
  error += EXPECTED("Interpolation coeficient A.x", c.a.x, -1.35714, 1e-4);
  error += EXPECTED("Interpolation coeficient A.y", c.a.y, -5.59502, 1e-4);
  error += EXPECTED("Interpolation coeficient B.x", c.b.x, 2.35714, 1e-4);
  error += EXPECTED("Interpolation coeficient B.y", c.b.y, 9.473, 1e-4);
  error += EXPECTED("Interpolation coeficient C.x", c.c.x, 0.0, 1e-4);
  error += EXPECTED("Interpolation coeficient C.y", c.c.y, 0.0, 1e-4);
  error += EXPECTED("Interpolation coeficient D.x", c.d.x, 1.0, 1e-4);
  error += EXPECTED("Interpolation coeficient D.y", c.d.y, 3.24534, 1e-4);
  c = ip.getCoefs(1);
  error += EXPECTED("Interpolation coeficient A.x", c.a.x, -0.928571, 1e-4);
  error += EXPECTED("Interpolation coeficient A.y", c.a.y, -0.826156, 1e-4);
  error += EXPECTED("Interpolation coeficient B.x", c.b.x, 1.28571, 1e-4);
  error += EXPECTED("Interpolation coeficient B.y", c.b.y, 1.65386, 1e-4);
  error += EXPECTED("Interpolation coeficient C.x", c.c.x, 0.642857, 1e-4);
  error += EXPECTED("Interpolation coeficient C.y", c.c.y, 2.16094, 1e-4);
  error += EXPECTED("Interpolation coeficient D.x", c.d.x, 2.0, 1e-4);
  error += EXPECTED("Interpolation coeficient D.y", c.d.y, 7.12332, 1e-4);
  c = ip.getCoefs(2);
  error += EXPECTED("Interpolation coeficient A.x", c.a.x, -0.928571, 1e-4);
  error += EXPECTED("Interpolation coeficient A.y", c.a.y, 9.58966, 1e-4);
  error += EXPECTED("Interpolation coeficient B.x", c.b.x, 1.5, 1e-4);
  error += EXPECTED("Interpolation coeficient B.y", c.b.y, -18.4575, 1e-4);
  error += EXPECTED("Interpolation coeficient C.x", c.c.x, 0.428571, 1e-4);
  error += EXPECTED("Interpolation coeficient C.y", c.c.y, 2.99019, 1e-4);
  error += EXPECTED("Interpolation coeficient D.x", c.d.x, 3.0, 1e-4);
  error += EXPECTED("Interpolation coeficient D.y", c.d.y, 10.112, 1e-4);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
