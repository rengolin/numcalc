#include <iostream>
#include "../EquationParser.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Polynomials test
int main () {
  unsigned error = 0;
  cout << "Equation Parser test: ";

  // Constant
  EquationParser cons("3");
  error += EXPECTED("3", cons.eval(), 3.0);

  EquationParser cons2("3 / 2");
  error += EXPECTED("3/2", cons2.eval(), 1.5);

  EquationParser cons3("3 + 2 - (4 * 2)");
  error += EXPECTED("3+2-(4*2)", cons3.eval(), -3.0);

  EquationParser cons4("(3 / 2) - (1 * 2)");
  error += EXPECTED("(3/2)-(1*2)", cons4.eval(), -0.5);

  EquationParser cons5("((3 / 2) * 4) - (1 * 2)");
  error += EXPECTED("((3/2)*4)-(1*2)", cons5.eval(), 4.0);

  EquationParser fp("3.0");
  error += EXPECTED("3.0", fp.eval(), 3.0);

  EquationParser fp2("3.0e2");
  error += EXPECTED("3.0e2", fp2.eval(), 300.0);

  EquationParser fp3("-3.0");
  error += EXPECTED("-3.0", fp3.eval(), -3.0);

  EquationParser fp4("3.0e-2");
  error += EXPECTED("3.0e-2", fp4.eval(), 0.03);

  // Linear
  EquationParser var("a");
  error += EXPECTED("a at [-1]", var.eval(-1), -1.0);
  error += EXPECTED("a at [0]", var.eval(0), 0.0);
  error += EXPECTED("a at [1]", var.eval(1), 1.0);

  EquationParser line("a - 7");
  error += EXPECTED("a - 7 at [-5]", line.eval(-5), -12.0);
  error += EXPECTED("a - 7 at [0]", line.eval(0), -7.0);
  error += EXPECTED("a - 7 at [5]", line.eval(5), -2.0);

  EquationParser line2("5 + var - 7");
  error += EXPECTED("5 + var - 7 at [-10]", line2.eval(-10), -12.0);
  error += EXPECTED("5 + var - 7 at [0]", line2.eval(0), -2.0);
  error += EXPECTED("5 + var - 7 at [10]", line2.eval(10), 8.0);

  EquationParser line3("5 - var * 7");
  error += EXPECTED("5 - var * 7 at [-1]", line3.eval(-1), 12.0);
  error += EXPECTED("5 - var * 7 at [0]", line3.eval(0), 5.0);
  error += EXPECTED("5 - var * 7 at [1]", line3.eval(1), -2.0);

  EquationParser line4("(5 * var) - 7");
  error += EXPECTED("(5 * var) - 7 at [-1]", line4.eval(-1), -12.0);
  error += EXPECTED("(5 * var) - 7 at [-1]", line4.eval(0), -7.0);
  error += EXPECTED("(5 * var) - 7 at [-1]", line4.eval(1), -2.0);

  // Higher degree poly
  EquationParser poly1("(3 * (x ^ 2)) + (5 * x) - 7");
  error += EXPECTED("(3 * (x ^ 2)) + (5 * x) - 7 at [-1]", poly1.eval(-1), -9.0);
  error += EXPECTED("(3 * (x ^ 2)) + (5 * x) - 7 at [0]", poly1.eval(0), -7.0);
  error += EXPECTED("(3 * (x ^ 2)) + (5 * x) - 7 at [1]", poly1.eval(1), 1.0);

  // Multiple variables
  EquationParser multi("(5 * foo) - (7 * bar)");
  map<string, double> multiMap;
  multiMap["foo"] = 1; multiMap["bar"] = -1;
  error += EXPECTED("(5 * foo) - (7 * bar) at [1, -1]", multi.eval(multiMap), 12.0);
  multiMap["foo"] = 0; multiMap["bar"] = 0;
  error += EXPECTED("(5 * foo) - (7 * bar) at [0, 0]", multi.eval(multiMap), 0.0);
  multiMap["foo"] = -1; multiMap["bar"] = 1;
  error += EXPECTED("(5 * foo) - (7 * bar) at [-1, 1]", multi.eval(multiMap), -12.0);

  EquationParser multi2("(5 * foo) / (7 * bar)");
  map<string, double> multi2Map;
  multi2Map["foo"] = 1; multi2Map["bar"] = 1;
  error += EXPECTED("(5 * foo) / (7 * bar) at [1, 1]", multi2.eval(multi2Map), 5.0/7.0);
  multi2Map["foo"] = 0; multi2Map["bar"] = 1;
  error += EXPECTED("(5 * foo) / (7 * bar) at [0, 1]", multi2.eval(multi2Map), 0.0);
  multi2Map["foo"] = 1; multi2Map["bar"] = 10;
  error += EXPECTED("(5 * foo) / (7 * bar) at [1, 10]", multi2.eval(multi2Map), 5.0/70.0);

  // Functions
  EquationParser sin1("sin(1.0)");
  error += EXPECTED("sin(1.0)", sin1.eval(), 0.84147);

  EquationParser sin2("sin((3.14159265359 / 2) * x)");
  error += EXPECTED("sin(pi/2*x) at [0]", sin2.eval(0), 0.0);
  error += EXPECTED("sin(pi/2*x) at [1]", sin2.eval(1), 1.0);
  error += EXPECTED("sin(pi/2*x) at [2]", sin2.eval(2), 0.0);
  error += EXPECTED("sin(pi/2*x) at [3]", sin2.eval(3), -1.0);

  EquationParser cos1("cos((3.14159265359 / 2) * x)");
  error += EXPECTED("cos(pi/2*x) at [0]", cos1.eval(0), 1.0);
  error += EXPECTED("cos(pi/2*x) at [1]", cos1.eval(1), 0.0);
  error += EXPECTED("cos(pi/2*x) at [2]", cos1.eval(2), -1.0);
  error += EXPECTED("cos(pi/2*x) at [3]", cos1.eval(3), 0.0);

  EquationParser tan1("tan((3.14159265359 / 3) * x)");
  error += EXPECTED("tan(pi/3*x) at [0]", tan1.eval(0), 0.0);
  error += EXPECTED("tan(pi/3*x) at [1]", tan1.eval(1), 1.732050);
  error += EXPECTED("tan(pi/3*x) at [2]", tan1.eval(2), -1.732050);

  EquationParser exp1("exp(x)");
  error += EXPECTED("exp(x) at [-1]", exp1.eval(-1), 0.367879);
  error += EXPECTED("exp(x) at [0]", exp1.eval(0), 1.0);
  error += EXPECTED("exp(x) at [1]", exp1.eval(1), 2.718281);

  EquationParser ln1("ln(x)");
  error += EXPECTED("ln(x) at [0.5]", ln1.eval(0.5), -0.693147);
  error += EXPECTED("ln(x) at [1]", ln1.eval(1), 0.0);
  error += EXPECTED("ln(x) at [2]", ln1.eval(2), 0.693147);

  EquationParser sqrt1("sqrt(x)");
  error += EXPECTED("sqrt(x) at [0]", sqrt1.eval(0), 0.0);
  error += EXPECTED("sqrt(x) at [2]", sqrt1.eval(2), 1.414213);
  error += EXPECTED("sqrt(x) at [4]", sqrt1.eval(4), 2.0);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
