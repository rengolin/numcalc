#include <iostream>
#include "../Poly.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Polynomials test
int main () {
  unsigned error = 0;
  cout << "Polynomial test: ";

  // Constant
  Poly cons({3.0});
  error += EXPECTED("y = 3 at [-1]", cons(-1), 3.0);
  error += EXPECTED("y = 3 at [ 0]", cons(0), 3.0);
  error += EXPECTED("y = 3 at [+1]", cons(1), 3.0);
  // Linear
  Poly line({5.0, -2.0});
  error += EXPECTED("y = -2x + 5 at [-5]", line(-5), 15.0);
  error += EXPECTED("y = -2x + 5 at [ 2]", line(2), 1.0);
  error += EXPECTED("y = -2x + 5 at [10]", line(10), -15.0);
  // Quadratic
  Poly quad({1.0, -1.0, 1.0});
  error += EXPECTED("y = x^2 - x + 1 at [-2]", quad(-2), 7.0);
  error += EXPECTED("y = x^2 - x + 1 at [ 1]", quad(1), 1.0);
  error += EXPECTED("y = x^2 - x + 1 at [ 5]", quad(5), 21.0);

  // Examples
  Poly a({1.0, 1.0, 5.0, 1.0, 6.0});
  error += EXPECTED("y = 6x^4 + x^3 + 5x^2 + x + 1 at [1/3]", a(0.333333), 2.0);
  Poly b({1.0, -5.0, 5.0, 4.0, -3.0});
  error += EXPECTED("y = -3x^4 + 4x^3 + 5x^2 - 5x + 1 at [1/3]", b(0.333333), 2.5926e-07);
  Poly c({1.0, 0.0, -1.0, 1.0, 2.0});
  error += EXPECTED("y = 2x^4 + x^3 - x^2 + 1 at [1/3]", c(0.333333), 0.950617);


  // Find errors
  vector<double> fifty;
  for (int i=0; i<50; ++i) fifty.push_back(1);
  double x = 1.00001;
  Poly Fifty(fifty);
  double Exp = Fifty(x);
  double Quick = (pow(x, 51) - 1)/(x - 1);
  error += EXPECTED("Error between compressed and full evaluation (in %)",
                    (Quick/Exp - 1.0) * 100,
                    2.00051);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
