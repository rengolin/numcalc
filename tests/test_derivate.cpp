#include <iostream>
#include "../Derivate.h"
#include "test.h"

using namespace std;
using namespace numcalc;

int main() {
  unsigned error = 0;
  cout << "Derivation test: ";

  // f'(x) = 2x+3
  Derivate d1([](double x) { return x * x + 3 * x - 4; });
  error += EXPECTED("\t(x^2+3x-4)'(-1) ", d1(-1), 1.0);
  error += EXPECTED("\t(x^2+3x-4)'(0) ", d1(0), 3.0);
  error += EXPECTED("\t(x^2+3x-4)'(1) ", d1(1), 5.0);

  // Change prepcision
  Derivate d2([](double x) { return x * x + 3 * x - 4; }, 0.1, 5);
  error += EXPECTED("\t(x^2+3x-4)'(-1) ", d2(-1), 1.0);
  error += EXPECTED("\t(x^2+3x-4)'(0) ", d2(0), 3.0);
  error += EXPECTED("\t(x^2+3x-4)'(1) ", d2(1), 5.0);

  // f'(x) = cos(x)
  Derivate d3([](double x) { return sin(x); });
  for (double i=-M_PI; i<2*M_PI; i+=M_PI/2)
    error += EXPECTED("\t(sin(x))'", d3(i), cos(i));

  // f'(x) = e^x
  Derivate d4([](double x) { return exp(x); });
  for (double i=0.1; i<2.0; i+=0.2)
    error += EXPECTED("\t(e^x)' ", d4(i), exp(i));

  // f''(x) = 2
  SecondDerivate sd1([](double x) { return x * x + 3 * x - 4; });
  error += EXPECTED("\t(x^2+3x-4)''(-1) ", sd1(1), 2.0);
  error += EXPECTED("\t(x^2+3x-4)''(0) ", sd1(1), 2.0);
  error += EXPECTED("\t(x^2+3x-4)''(1) ", sd1(1), 2.0);

  // Change prepcision
  SecondDerivate sd2([](double x) { return x * x + 3 * x - 4; }, 0.1, 100);
  error += EXPECTED("\t(x^2+3x-4)''(-1) ", sd2(-1), 2.0);
  error += EXPECTED("\t(x^2+3x-4)''(0) ", sd2(0), 2.0);
  error += EXPECTED("\t(x^2+3x-4)''(1) ", sd2(1), 2.0);

  // f''(x) = -sin(x)
  SecondDerivate sd3([](double x) { return sin(x); });
  for (double i=-M_PI; i<2*M_PI; i+=M_PI/2)
    error += EXPECTED("\t(sin(x))'", sd3(i), -sin(i));

  // f''(x) = e^x
  SecondDerivate sd4([](double x) { return exp(x); });
  for (double i=0.1; i<2.0; i+=0.2)
    error += EXPECTED("\t(e^x)' ", sd4(i), exp(i));

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
