#include "../BestFit.h"
#include "test.h"

using namespace std;
using namespace numcalc;

// Maths test
int main() {
  unsigned error = 0;
  cout << "BestFit test: ";

  matrix_t A({{1, 1},{1, -1},{1, 1}});
  vector_t b({2, 1, 3});
  LeastSquares LS(A, b);
  error += EXPECTED("Least Squares error", LS.getError(), 0.408248);

  matrix_t B({{1, -4},{2, 3},{2, 2}});
  vector_t c({-3, 15, 9});
  LeastSquares LS2(B, c);
  error += EXPECTED("Least Squares error", LS2.getError(), sqrt(3));

  points_t P({{1, 2},{-1, 1},{1, 3}});
  PolyFit LF(P, 1);
  error += EXPECTED("Linear fit error", LF.getError(), LS.getError());
  error += EXPECTED("Linear fit coef[0]", LF.getCoef(0), 1.75);
  error += EXPECTED("Linear fit coef[1]", LF.getCoef(1), 0.75);

  points_t P2({{-1, 1},{0, 0},{1, 0},{2, -2}});
  PolyFit LF2(P2, 1);
  error += EXPECTED("Linear fit error", LF2.getError(), 0.418330);
  error += EXPECTED("Linear fit coef[0]", LF2.getCoef(0),  0.2);
  error += EXPECTED("Linear fit coef[1]", LF2.getCoef(1), -0.9);
  PolyFit SF2(P2, 2);
  error += EXPECTED("Square fit error", SF2.getError(), 0.335410);
  error += EXPECTED("Square fit coef[0]", SF2.getCoef(0),  0.45);
  error += EXPECTED("Square fit coef[1]", SF2.getCoef(1), -0.65);
  error += EXPECTED("Square fit coef[2]", SF2.getCoef(2), -0.25);
  PolyFit SF3(P2, 3);
  error += EXPECTED("Cubic fit error", SF3.getError(), 0.0);
  error += EXPECTED("Cubic fit coef[0]", SF3.getCoef(0),  0.00);
  error += EXPECTED("Cubic fit coef[1]", SF3.getCoef(1),  0.00);
  error += EXPECTED("Cubic fit coef[2]", SF3.getCoef(2),  0.50);
  error += EXPECTED("Cubic fit coef[3]", SF3.getCoef(3), -0.50);

  points_t PP({{0./8, -2.2},{1./8, -2.8},{2./8, -6.1},{3./8, -3.9},
               {4./8,  0.0},{5./8,  1.1},{6./8, -0.6},{7./8, -1.1},
               {8./8, -2.2}}); // Last = First -> Periodic
  PeriodicFit PF(PP, 1);
  error += EXPECTED("Periodic fit error", PF.getError(), 1.012081);
  error += EXPECTED("Periodic fit coef[0]", PF.getCoef(0), -1.90505);
  error += EXPECTED("Periodic fit coef[1]", PF.getCoef(1), -0.65455);
  error += EXPECTED("Periodic fit coef[2]", PF.getCoef(2), -2.5594);
  PeriodicFit PF2(PP, 2);
  error += EXPECTED("Periodic2 fit error", PF2.getError(), 0.408334);
  error += EXPECTED("Periodic2 fit coef[0]", PF2.getCoef(0), -1.998504);
  error += EXPECTED("Periodic2 fit coef[1]", PF2.getCoef(1), -0.841461);
  error += EXPECTED("Periodic2 fit coef[2]", PF2.getCoef(2), -2.559404);
  error += EXPECTED("Periodic2 fit coef[3]", PF2.getCoef(3), 1.027993);
  error += EXPECTED("Periodic2 fit coef[4]", PF2.getCoef(4), 0.825000);
  PeriodicFit PF3(PP, 3);
  error += EXPECTED("Periodic3 fit error", PF3.getError(), 0.267775);

  points_t PE({{0, 53.05},{5, 73.04},{10, 98.31},{15, 139.78},
               {20, 193.48},{25, 260.20},{30, 320.39}});
  LogarithmFit ELF(PE, LogFitType::EXP);
  error += EXPECTED("ExpLog fit error", ELF.getError(), 0.035692);
  error += EXPECTED("ExpLog fit coef[0]", ELF.getCoef(0), 3.989567);
  error += EXPECTED("ExpLog fit coef[1]", ELF.getCoef(1), 0.061520);

  points_t PPE({{0.9120, 13.7},{0.9860, 15.9},{1.0600, 18.5},
                {1.1300, 21.3},{1.1900, 23.5},{1.2600, 27.2},
                {1.3200, 32.7},{1.3800, 36.0},{1.4100, 38.6},
                {1.4900, 43.7}});
  LogarithmFit PLF(PPE, LogFitType::PWR);
  error += EXPECTED("PowerLog fit error", PLF.getError(), 0.033044);
  error += EXPECTED("PowerLog fit coef[0]", PLF.getCoef(0), 2.791433);
  error += EXPECTED("PowerLog fit coef[1]", PLF.getCoef(1), 2.419859);

  points_t HLE({{1,  8.0},{2, 12.3},{3, 15.5},{4, 16.8},
                {5, 17.1},{6, 15.8},{7, 15.2},{8, 14.0}});
  LogarithmFit HLF(HLE, LogFitType::HL);
  error += EXPECTED("HalfLifeLog fit error", HLF.getError(), 0.018333);
  error += EXPECTED("HalfLifeLog fit coef[0]", HLF.getCoef(0), 2.281378);
  error += EXPECTED("HalfLifeLog fit coef[1]", HLF.getCoef(1), -0.215137);

  // ====================== BestFit: Polynomial
  {
    BestFit BF(P2);
    Fit *Best = BF.getBest();
    vector_t expectedCoefs({0.0, 0.0, 0.5, -0.5});
    error += EXPECTED("Best fit error", Best->getError(), 0.0);
    PolyFit *PF = dynamic_cast<PolyFit*>(Best);
    if (!PF)
      ERROR("Best fit should be polynomial");
    else {
      EXPECTED("Degree", PF->getDegree(), 3);
      for (unsigned i=0; i<PF->getDegree(); i++)
        error += EXPECTED("Best fit coef", PF->getCoef(i), expectedCoefs[i]);
    }
  }

  // ====================== BestFit: Periodic
  {
    BestFit BF(PP);
    Fit *Best = BF.getBest();
    vector_t expectedCoefs({-1.968333, -0.781121, -2.559404, 1.088333,
                             0.825000, -0.392212,  0.190596});
    error += EXPECTED("Periodic3 fit error", Best->getError(), 0.267775);
    PeriodicFit *PF = dynamic_cast<PeriodicFit*>(Best);
    if (!PF)
      ERROR("Best fit should be periodic");
    else {
      EXPECTED("Degree", PF->getDegree(), 3);
      for (unsigned i=0; i<=PF->getDegree()*2; i++)
        error += EXPECTED("Best fit coef", PF->getCoef(i), expectedCoefs[i]);
    }
  }

  // ====================== BestFit: Logarithmic
  {
    BestFit BF(PPE);
    Fit *Best = BF.getBest();
    vector_t expectedCoefs({0.737500, 2.055273});
    error += EXPECTED("Best fit error", Best->getError(), 0.018933);
    LogarithmFit *LF = dynamic_cast<LogarithmFit*>(Best);
    if (!LF)
      ERROR("Best fit should be logarithmic");
    else {
      error += EXPECTED("Best fit coef", LF->getCoef(0), expectedCoefs[0]);
      error += EXPECTED("Best fit coef", LF->getCoef(1), expectedCoefs[1]);
    }
  }

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
