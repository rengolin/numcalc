#include <iostream>
#include "../Integrate.h"
#include "test.h"

using namespace std;
using namespace numcalc;

int main() {
  unsigned error = 0;
  cout << "Integration test: ";

  // compile with c++11 support
  Integrate it([](double x) { return x * x + 3 * x - 4; });
  error += EXPECTED("\tIntegral is ", it.sum(0, 1), -2.16797);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
