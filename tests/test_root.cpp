#include <iostream>
#include "../RootFinder.h"
#include "test.h"

using namespace std;
using namespace numcalc;

int main () {
  unsigned error = 0;
  cout << "Root finder test: ";

  // compile with c++11 support
  RootFinder rf([](double x) { return x * x + 3 * x - 4; });

  // Find lower root
  error += EXPECTED("Lower Root is (Brent)", rf.find(-9.8), -4.0);
  error += EXPECTED("Brent steps", rf.getSteps(), 5);
  error += EXPECTED("Lower Root is (Secant)", rf.secantFind(-9.8), -4.0);
  error += EXPECTED("Secant steps", rf.getSteps(), 12);
  error += EXPECTED("Lower Root is (Bisect)", rf.bisectFind(-9.8), -4.0);
  error += EXPECTED("Bisect steps", rf.getSteps(), 34);

  // Find upper error
  error += EXPECTED("Upper Root is (Brent)", rf.find(7.6), 1.0);
  error += EXPECTED("Brent steps", rf.getSteps(), 6);
  error += EXPECTED("Lower Root is (Secant)", rf.secantFind(7.6), 1.0);
  error += EXPECTED("Secant steps", rf.getSteps(), 11);
  error += EXPECTED("Upper Root is (Bisect)", rf.bisectFind(7.6), 1.0);
  error += EXPECTED("Bisect steps", rf.getSteps(), 35);

  if (!error) {
    cout << "PASS" << endl;
  } else {
    cout << "ERROR" << endl;
  }
  return error;
}
