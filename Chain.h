#ifndef CHAIN_H_
#define CHAIN_H_
#include <set>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/adjacency_list.hpp>

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

/*
 * This class implements a classic Markov Chain through the use of an
 * adjacency list based graph (provided by booth).
 * The model is extremely simple: (vertex A) --(edge)--> (vertex B) with the
 * vertex type provided by the template and the edge type as a simple count.
 *
 * Random walks are simply iterating through the out edges and picking an
 * edge at random. Control of starting/ending nodes as well as number of steps
 * is left for the user of the class.
 */
template <class vertex_t>
class Chain {
  // Basic Markov counter
  struct edge_t { int count; };

  // directed weight graph
  typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS, vertex_t, edge_t > graph_t;
  // Vertex and Edge descriptors
  typedef typename boost::graph_traits < graph_t >::vertex_descriptor vertex_d;
  typedef typename boost::graph_traits < graph_t >::edge_descriptor edge_d;
  typedef typename boost::graph_traits < graph_t >::out_edge_iterator edge_i;
  // Map with all vertexes and links (deduplicate)
  typedef typename std::map < vertex_t, vertex_d > vertex_map_t;
  typedef typename vertex_map_t::iterator vertex_map_i;

  // Graph itself
  graph_t graph;
  // List of links
  vertex_map_t links;
  // Current point
  vertex_d current;

  // Add a new vertex
  vertex_d addVertex(vertex_t value) {
    vertex_map_i pos;
    vertex_d descriptor;
    bool inserted;

    // Insert into map
    boost::tie(pos, inserted) = links.insert(make_pair(value, vertex_d()));

    // Insert into graph (or get descriptor)
    if (inserted) {
      descriptor = add_vertex(value, graph);
      pos->second = descriptor; // pos is persistent
      DEBUG("      New value [" << value << "], descriptor ["
                                << descriptor << "]");
    } else {
      descriptor = pos->second;
      DEBUG("      Old value [" << value << "], descriptor ["
                                << descriptor << "]");
    }

    return descriptor;
  }

  // Add an edge between two descriptors
  bool addEdge(vertex_d from, vertex_d to, int count) {
    bool inserted, found;
    edge_d link;
    boost::tie(link, found) = edge(from, to, graph);

    if (found) {
      graph[link].count += count;
      inserted = true;
      DEBUG("      Edge found, count [" << graph[link].count << "]");
    } else {
      edge_t e = { count };
      boost::tie(link, inserted) = add_edge(from, to, e, graph);
      DEBUG("      New edge, count [" << graph[link].count << "]");
    }
    return inserted;
  }

  // Get specific vertex descriptor
  vertex_d getDescriptor(vertex_t value) {
    return links[value];
  }

  // Set current link
  void setCurrent(vertex_d value) {
    current = value;
  }

public:
  Chain() : current(0) {
    srand(time(NULL));
  }

  Chain(int seed) : current(0) {
    srand(seed);
  }

  // Add connection (or increase cardinality of existing ones)
  bool add(vertex_t source, vertex_t target, int count=1) {
    DEBUG("    Adding a new tuple...");
    vertex_d from = addVertex(source);
    vertex_d to = addVertex(target);
    return addEdge(from, to, count);
  }

  // Walk down the next hop
  vertex_t next() {
    DEBUG("    Next node...");
    edge_i from, to, cur;

    // All edges from current point
    boost::tie(from, to) = out_edges(current, graph);
    DEBUG("    Out edges: " << (to-from));

    // Add up total count
    int totalCount = 0;
    for (cur = from; cur != to; ++cur)
      totalCount += graph[(*cur)].count;
    DEBUG("    Total count: " << totalCount);

    // No out edges
    if (totalCount == 0)
      return getCurrent();

    // Get random position
    int randCount = random() % totalCount;
    int currCount = 0;
    for (cur = from; cur != to; ++cur) {
      if (currCount + graph[(*cur)].count > randCount)
        break;
      currCount += graph[(*cur)].count;
      DEBUG("      Current count [" << graph[target(*cur, graph)] << "]: " << currCount);
    }
    vertex_d t = target(*cur, graph);
    setCurrent(t);
    DEBUG("    Next node is: " << getCurrent());

    return getCurrent();
  }

  // Get current vertex
  vertex_t getCurrent() {
    return graph[current];
  }
};

} // namespace numcalc

#endif /*CHAIN_H_*/
