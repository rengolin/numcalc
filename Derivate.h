#ifndef DERIVATE_H_
#define DERIVATE_H_
#include <cassert>
#include <functional>

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

/*
 * Derivate functions numerically, using the three-point rule:
 *  f'(x) = [ f(x+h) - f(x-h) ] / 2h
 * with error:
 *  (h/2).f''(x-h) < e < (h/2).f''(x+h)
 * which cannot be calculated directly.
 *
 * Ch.5, Numerical Analysis, T. Sauer, Pearson
 */
class Derivate {
  std::function<double (double)> f;
  const double h;
  const double invh; // 1/2h
public:
  Derivate(std::function<double (double)> f, double h, double invh) :
    f(f), h(h), invh(invh) {
    // Make sure it's either both or nothing
    assert(h && invh);
  }
  Derivate(std::function<double (double)> f) : Derivate(f, 1e-4, 5e3) { }
  double operator() (double x) {
    DEBUG("  x = " << x);
    DEBUG("  h = " << h);
    DEBUG("  f(x-h) = " << f(x-h));
    DEBUG("  f(x+h) = " << f(x+h));
    return (f(x+h) - f(x-h)) * invh;
  }
};

/*
 * Second derivative, using three-point centered-difference rule:
 *  f''(x) = [ f(x-h} - 2.f(x) + f(x+h) ] / h^2
 * with error:
 *  (h^2/12).f''''(x-h) < e < (h^2/12).f''''(x+h)
 * which cannot be calculated directly.
 *
 * Ch.5, Numerical Analysis, T. Sauer, Pearson
 */
class SecondDerivate {
  std::function<double (double)> f;
  const double h;
  const double invh; // 1/h^2
public:
  SecondDerivate(std::function<double (double)> f, double h, double invh) :
    f(f), h(h), invh(invh) {
    // Make sure it's either both or nothing
    assert(h && invh);
  }
  SecondDerivate(std::function<double (double)> f) :
    SecondDerivate(f, 1e-4, 1e8) { }
  double operator() (double x) {
    DEBUG("  x = " << x);
    DEBUG("  h = " << h);
    DEBUG("  f(x-h) = " << f(x-h));
    DEBUG("  2*f(x) = " << 2*f(x));
    DEBUG("  f(x+h) = " << f(x+h));
    return (f(x-h) - 2*f(x) + f(x+h)) * invh;
  }
};

} // namespace numcalc

#endif // DERIVATE_H_
