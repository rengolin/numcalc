#include <cassert>
#include <cmath>
#include <limits>
#include "RootFinder.h"
#include "Math.h"

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

// Expands the bracket until inverted values of f(x) are found for the function,
// guaranteeing the existence of at least one root in that range. This method
// is not guaranteed to fund intervals, for example, when searching over
// trigonometric functions and the bracket extension has the same length as
// the period of the function.
bool
RootFinder::findBracket(double *a, double *b) {
  double fa = (*f)(*a);
  double fb = (*f)(*b);
  double _fa;
  unsigned d = depth;
  DEBUG("\t\tBracket: [" << *a << ", " << *b << "] -> "
                     "[" << fa << ", " << fb << "]");

  if (inverted(fa, fb))
    return true;

  while (d--) {
    *a -= bracket;
    *b += bracket;
    _fa = fa;
    fa = (*f)(*a);
    fb = (*f)(*b);
    DEBUG("\t\tBracket: [" << *a << ", " << *b << "] -> "
                       "[" << fa << ", " << fb << "]");

    if (!inverted(fa, fb))
      continue;

    // Now it's inverted, and it has to be in one of the end brackets
    // either to the left or to the right. This way we can reduce considerably
    // the search space to a single bracket.
    if (inverted(fa, _fa))
      *b = *a + bracket;
    else
      *a = *b - bracket;
#ifdef _DEBUG
    fa = (*f)(*a);
    fb = (*f)(*b);
    DEBUG("\t\t  Final: [" << *a << ", " << *b << "] -> "
                       "[" << fa << ", " << fb << "]");
#endif
    return true;
  }

  // Didn't find good bracket
  return false;
}

// Implementing Brent's Inverse Quadratic method, interpolating the two edges of
// the bracket with an quadratic curve. This is the best approximation, and can
// find roots much faster than the other two methods, but it still doesn't
// guarantee stability or reduction of the error margin for all cases.
double
RootFinder::find(double x) {
  // Iterations
  unsigned d = depth;
  // Extra points
  double x1 = x + bracket, x2 = x - bracket;

  // Make sure we can find a proper bracket
  if (!findBracket(&x2, &x1))
    return std::numeric_limits<double>::quiet_NaN();

  // Function evaluation
  double fx;
  double fx1 = (*f)(x1);
  double fx2 = (*f)(x2);
  // Lucky find?
  if (isZero(fx1))
    return x1;
  if (isZero(fx2))
    return x2;

  // Find root
  while (d--) {
    // Function evaluation
    fx = (*f)(x);

    // Found!
    if (isZero(fx)) {
      steps = depth - d;
      return x;
    }

    // Get temp vars
    double R = fx1 / fx;
    double S = fx1 / fx2;
    double T = fx2 / fx;
    double P = S * ( T * (R - T) * (x - x1) - (1 - R) * (x1 - x2) );
    double Q = (R - 1) * (S -1) * (T - 1);

    // Save old
    x2 = x1;
    x1 = x;
    fx2 = fx1;
    fx1 = fx;

    // Set new X
    x = x2 + P / Q;

    // Check if values still valid
    assert(!std::isnan(x) && !std::isinf(x));
  }

  // Didn't find the root
  return std::numeric_limits<double>::quiet_NaN();
}

// This is a simplification of the Newton-Raphson method that, instead of
// using tangents (which need the first derivative), it uses secants, which
// are just lines from the two edges of the bracet.
// It can converge faster than bisection for smooth functions where the
// tangent is inclined (ie. not hovering over the X axis).
double
RootFinder::secantFind(double x) {
  // Iterations
  unsigned d = depth;

  // Find brackets
  double a = x - bracket, b = x + bracket, fx;

  // Make sure we can find a proper bracket
  if (!findBracket(&a, &b))
    return std::numeric_limits<double>::quiet_NaN();

  double fa = (*f)(a);
  double fb = (*f)(b);

  // Lucky find?
  if (isZero(fa))
    return a;
  if (isZero(fb))
    return b;

  // Find root
  while (d--) {
    // Secant
    x = a - (fa * (a - b)) / (fa - fb);

    // Found?
    fx = (*f)(x);
    DEBUG("\t\tSecant: [" << x << ", " << fx << "]");
    if (isZero(fx)) {
      steps = depth - d;
      return x;
    }

    // Which brackets still inverted?
    if (inverted(fa, fx)) {
      b = x;
      fb = fx;
    } else if (inverted(fx, fb)) {
      a = x;
      fa = fx;
    } else {
      DEBUG("\t\tBisect: didn't invert?");
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  // Didn't find the root
  return std::numeric_limits<double>::quiet_NaN();
}

// This is the simplest method possible, but guaranteed to converge to "a"
// root once the interval has inverted values of f(x). If more than one root
// exists in the interval, changes in the initial beacket can change which root
// is found.
double
RootFinder::bisectFind(double x) {
  // Iterations
  unsigned d = depth;

  // Find brackets
  double a = x - bracket, b = x + bracket, fx;

  // Make sure we can find a proper bracket
  if (!findBracket(&a, &b))
    return std::numeric_limits<double>::quiet_NaN();

  double fa = (*f)(a);
  double fb = (*f)(b);

  // Lucky find?
  if (isZero(fa))
    return a;
  if (isZero(fb))
    return b;


  // Find root
  while (d--) {
    // Bissection
    x = (b + a) / 2;

    // Found?
    fx = (*f)(x);
    DEBUG("\t\tBisect: [" << x << ", " << fx << "]");
    if (isZero(fx)) {
      steps = depth - d;
      return x;
    }

    // Which brackets still inverted?
    if (inverted(fa, fx)) {
      b = x;
      fb = fx;
    } else if (inverted(fx, fb)) {
      a = x;
      fa = fx;
    } else {
      DEBUG("\t\tBisect: didn't invert?");
      return std::numeric_limits<double>::quiet_NaN();
    }
  }

  // Didn't find the root
  return std::numeric_limits<double>::quiet_NaN();
}

} // namespace numcalc
