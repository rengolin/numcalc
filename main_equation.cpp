#include <iostream>
#include <fstream>
#include <vector>
#include "EquationParser.h"

using namespace std;
using namespace numcalc;

// Usage: cat equation.txt | equation
// Returns points from A to B in Delta increments
int main() {
  string eq;
  getline(cin, eq);
  EquationParser parser(eq);

  if (!parser.parse()) {
    cerr << "ERROR: Cannot parse equation" << endl;
    cerr << " -> '" << eq << "'" << endl;
  }

  map<string, double> vars;
  string name;
  double value;
  for (unsigned i=0, max=parser.numVars(); i<max; i++) {
    cin >> name;
    cin >> value;
    vars.insert(make_pair(name, value));
  }

  double result = parser.eval(vars);
  cout << "Evaluation of equation: " << parser.getEquation() << endl;
  cout << "with variables:" << endl;
  for (auto v : vars)
    cout << "  " << v.first << " = " << v.second << endl;
  cout << "equals: [" << result << "]" << endl;

  return 0;
}
