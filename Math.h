#ifndef UTIL_H_
#define UTIL_H_
#include <iostream>
#include <cmath>
#include <vector>
#include <cassert>

namespace numcalc {

const double delta = 1e-10; // For isZero
const double step = 10*delta; // Step aside (max & min)
const double bracket = 1; // Bracket default size & increment

// ===================================== TYPES

// 2D Point Type: ( x, y )
struct point_t {
	double x;
	double y;
  friend std::ostream &operator<< (std::ostream &out, const point_t &p);
  // constructors
	point_t () : x(0), y(0) { }
	point_t (double X, double Y) : x(X), y(Y) { }
	point_t (const point_t& o) { x = o.x; y = o.y; }
  point_t& operator=(const point_t &o);
  // operators
  bool operator==(const point_t &o) const;
  bool operator!=(const point_t &o) const;
  point_t operator+(const point_t &o) const;
  point_t operator-(const point_t &o) const;
  point_t operator-() const;
  point_t operator*(const int a) const;
};
typedef std::vector<point_t> points_t;

// Vector Type: N-dimensional point type
class vector_t {
  std::vector<double> data;
  friend std::ostream &operator<< (std::ostream &out, const vector_t &v);
public:
  const unsigned len;
  // constructors
  vector_t() = delete;
  vector_t(unsigned N) : data(N), len(N) { }
  vector_t(std::vector<double>& V) : data(V), len(V.size()) { }
  vector_t(const vector_t& V) : data(V.data), len(V.len) { }
  vector_t(std::initializer_list<double> V) : data(V), len(V.size()) { }
  vector_t& operator=(const vector_t &o);
  // RW via (), RO via []
  double operator[](unsigned i) const;
  double& operator()(unsigned i);
  // operators
  bool operator==(const vector_t &o) const;
  bool operator!=(const vector_t &o) const;
  vector_t operator+(const vector_t &o) const;
  vector_t operator-(const vector_t &o) const;
  vector_t operator-() const;
  vector_t operator*(const int a) const;
};

// Matrix: NxM dimensional object type
class matrix_t {
  std::vector<double*> data;
  friend std::ostream &operator<< (std::ostream &out, const matrix_t &m);
public:
  const unsigned rows;
  const unsigned cols;
  // constructors
  matrix_t(unsigned rows, unsigned cols);
  matrix_t(const matrix_t &m);
  matrix_t(const std::initializer_list<std::initializer_list<double>>& list);
  ~matrix_t();
  // RW via (), RO via []
  const double* operator[] (unsigned i) const;
  double& operator() (unsigned r, unsigned c);
  // operations
  matrix_t& operator=(const matrix_t &o);
  bool operator==(const matrix_t &o) const;
  bool operator!=(const matrix_t &o) const;
  void swap(unsigned r1, unsigned r2);
  matrix_t operator+(const matrix_t &o) const;
  matrix_t operator-(const matrix_t &o) const;
  matrix_t operator*(const matrix_t &o) const;
  vector_t operator*(const vector_t &o) const;
  matrix_t operator*(const int a) const;
  matrix_t operator-() const;
  matrix_t transpose() const;
};

// Range: 1D vector
struct range_t {
  double start;
  double stop;
  range_t(const points_t &P, bool YAxis = false);
  range_t(const vector_t &V);
  double length() { return stop-start; }
};

// ===================================== UTILITIES

// Test for zero within defined quality (positive delta)
bool isZero(double x, double small = delta);

// Test if values have inverted signals
bool inverted(double x, double y);

} // namespace numcalc

#endif /*UTIL_H_*/
