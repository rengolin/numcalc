#ifndef _MATRIX_H_
#define _MATRIX_H_
#include <cassert>
#include "Math.h"

#ifdef _DEBUG
#include <iostream>
#define DEBUG(x) std::cout << x << std::endl;
#else
#define DEBUG(x)
#endif

namespace numcalc {

/*
 * This class implements the pivot-based normalisation of matrices of
 * dimensions NxM, with M = N+1. This is the representation of all
 * systems of equations of the form: Ax = b. By normalising |Ab|, one
 * can easily find x, which is what this class' method "getVar" does.
 *
 * Ch.2, Numerical Analysis, T. Sauer, Pearson
 */
class MatrixSolver {
  matrix_t M;
  std::vector<std::pair<bool,double>> vars;
  bool solved;
public:
  MatrixSolver (matrix_t &M) : M(M), vars(M.rows), solved(false) {
    // Make sure this is in canonical form
    assert(M.cols == M.rows + 1);
  }

  double getVar(unsigned idx) {
    assert(idx < M.rows);

    if (!solved)
      solve();

    // If cached, use it
    if (vars[idx].first)
      return vars[idx].second;

    // Make sure matrix is already diagonalised
    for (unsigned i=0; i<idx; ++i)
      assert(isZero(M(idx, i)));

    // ax + by = C -> ax = C - by -> x = (C - by)/a
    // Start with C...
    vars[idx].second = M(idx, M.rows);
    // Now subtract all 'by's (vars will be cached)
    for (unsigned i=idx+1; i<M.rows; ++i)
      vars[idx].second -= M(idx, i) * getVar(i);
    // Divide by 'a'
    vars[idx].second /= M(idx, idx);
    // Mark as cached
    vars[idx].first = true;

    return vars[idx].second;
  }

  // Gaussian Elimination, with additional line moving to reduce FP issues
  void solve() {
    // Partial pivoting, choose largest pivots
    for (unsigned row=0; row<M.rows-1; ++row) {
      unsigned largest = row;
      for (unsigned r=row+1; r<M.rows; ++r)
        if (std::abs(M(largest, row)) < std::abs(M(r, row)))
          largest = r;
      if (largest != row) {
        DEBUG("\t\tSwapping " << row << " with " << largest);
        M.swap(row, largest);
      }
    }
    // From top to bottom, use the pivot to match the current line's value
    for (unsigned row=0; row<M.rows-1; ++row) {
      double pivot = M(row, row);
      DEBUG("\t\tRow: " << row << ", pivot: " << pivot);
      assert(!isZero(pivot));
      // For each other row below...
      for (unsigned r=row+1; r<M.rows; ++r) {
        // Get correct multiplicative factor and...
        double mul = M(r, row) / pivot;
        DEBUG("\t\t\tMult = " << mul);
#ifdef _DEBUG
        std::cout << "\t\t\tBefore [" << r << "] = [";
        for (unsigned _d=0; _d<M.rows+1; ++_d)
          std::cout << " " << M(r, _d);
        std::cout << " ]" << std::endl;;
#endif
        // For each column to the right, incuding the constant
        for (unsigned c=row; c<=M.rows; c++) {
          // subtract from the current value
          M(r, c) = M(r, c) - mul * M(row, c);
          // Make sure the first cell actually became zero
          if (c == row)
            assert(isZero(M(r, c)));
        }
        // Now, cell (i, row) should be zero, and all others skewed.
        // Repeat, with a new pivot, and an adapted multiplicative factor.
#ifdef _DEBUG
        std::cout << "\t\t\t After [" << r << "] = [";
        for (unsigned _d=0; _d<M.rows+1; ++_d)
          std::cout << " " << M(r, _d);
        std::cout << " ]" << std::endl << std::endl;
#endif
      }
    }
    DEBUG("\t\tSolved matrix:" << std::endl << M);
    solved = true;
  }
};

} // namespace numcalc

#endif // _MATRIX_H_
