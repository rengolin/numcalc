#include <iostream>
#include <fstream>
#include <vector>
#include "MatrixSolver.h"

using namespace std;
using namespace numcalc;

// Usage: cat matrix.txt | matrix
// Solves matrix, print solution
int main() {
  // Reads file
  unsigned x, y;
  cin >> x;
  cin >> y;
  if (x > 10 || y > 9 || y != x+1) {
    cerr << "ERROR: Matrix file format:" << endl;
    cerr << "x y [ENTER] (matrix dimensions)" << endl;
    cerr << "x lines of y double values" << endl;
    return 1;
  }
  matrix_t M(x, y); // oops
  for (unsigned i=0; i<x; i++)
    for (unsigned j=0; j<y; j++)
      cin >> M(i, j);
  cout << "Original matrix is:" << endl << M << endl;

  MatrixSolver MS(M);
  for (unsigned i=0; i<x; i++)
    cout << "X(" << i << ") = " << MS.getVar(i) << endl;

  return 0;
}
