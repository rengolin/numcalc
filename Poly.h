#ifndef POLY_H_
#define POLY_H_
#include <cstdlib>
#include <vector>

namespace numcalc {

/*
 * This class uses the Horner's method, or nested
 * simplification, which reduces the number of
 * multiplications to solve a polynomial, ex:
 *
 * 2x^2 + 3x + 1
 *  to
 * 1 + x * (3 + x * (2))
 *
 * So, from least to most significant, C[i] is ith
 * power's coeficient:
 *
 * result = C[N-1]
 * for C[N-2:0] -> result = result * x + C[i]
 *
 * Ch.0, Numerical Analysis, T. Sauer, Pearson
 */
class Poly {
  std::vector<double>Coefs;
  unsigned Degree;
public:
  Poly(std::initializer_list<double> C) : Coefs(C), Degree(Coefs.size()) { }
  Poly(std::vector<double> C) : Coefs(C), Degree(Coefs.size()) { }

  double operator()(double x) {
    // Fast path
    if (Degree == 0)
      return 0.0;
    if (Degree == 1)
      return Coefs[0];
 
    // Iterations
    double result = Coefs[Degree-1];
    for (int i = Degree-2; i>=0; --i)
      result = result * x + Coefs[i];
 
    return result;
  }
};

} // namespace numcalc

#endif /*POLY_H_*/
