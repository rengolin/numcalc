#include <iostream>
#include <fstream>
#include <set>
#include "Chain.h"

using namespace std;
using namespace numcalc;

// Usage: cat chain.txt | chain
// Returns a random walk through the chain with no more than max nodes
int main () {
  unsigned pairs, max;
  cin >> pairs >> max;
  if (pairs > 1000000 || max > 1000000) {
    cerr << "ERROR: Chain file format:" << endl;
    cerr << "numpairs maxoutput (int int)" << endl;
    cerr << "from to (string string)" << endl;
    return 1;
  }

  // FIXME: Add support for quotes
  Chain <string> chain;
  for (unsigned i=0; i<pairs; i++) {
    string from, to;
    int count;
    cin >> from >> to >> count;
    chain.add(from, to, count);
  }

  // Following a random path based on number of connections
  unsigned steps = 0;
  cout << "Random walk:" << endl;
  do {
    string cur = chain.getCurrent();
    string last = cur;
    cout << cur << " ";
    cur = chain.next();
    if (cur == last)
      break;
  } while (steps++ < max-1);
  cout << endl;

  return 0;
}
