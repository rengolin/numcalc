#ifndef BESTFIT_H_
#define BESTFIT_H_
#include "Math.h"
#include "MatrixSolver.h"

namespace numcalc {

/*
 * Least squares: Ax = b, inconsistent
 * ie. dim[x] != dim[b], example:
 * |1 2 3|         |10|
 * |4 5 6| x |x| = |11|
 * |7 8 9|   |y|   |12|
 * 
 * Solution: Ax = b -> (A^T)A x' = (A^T)b
 *  -> dim[(A^T)A] = dim[x'] = dim[b]
 * 
 * RMS Error: sqrt( [r = b - Ax']^2 / dim[b] )
 * 
 * Ch.4, Numerical Analysis, T. Sauer, Pearson
 */
class LeastSquares {
  matrix_t A;
  vector_t b;
  double error;
  MatrixSolver *MS;
public:
  LeastSquares(const matrix_t &A, const vector_t &b);
  ~LeastSquares();
  double getCoef(unsigned i) { return MS->getVar(i); }
  double getError();
};

/*
 * Fit Base Class: common up least squares hadling and serve as
 * polymorphic type for BestFit.
 */
class Fit {
protected:
  LeastSquares *LS;
  vector_t coefs;
public:
  Fit(unsigned coefs) : coefs(coefs) { }
  virtual ~Fit() { if (LS) delete LS; }
  double getCoef(unsigned i) { assert(i < coefs.len); return LS->getCoef(i); }
  double getError() { return LS->getError(); }
};

/*
 * Poly Fit:
 *   A = | 1 x1 x1^2 ... x1^k |
 *       |        ...         |
 *       | 1 xn xn^2 ... xn^k |
 *   b = | y1 |
 *       | .. |
 *       | yn |
 *  x' = | c1 |
 *       | .. |
 *       | cn |
 * For approximation:
 *   c1 + c2 * x + c3 * x^2 + ... + cn * x^k
 */
class PolyFit : public Fit {
  unsigned degree;
public:
  PolyFit(const points_t &P, unsigned degree);
  unsigned getDegree() { return degree; }
};

/*
 * Periodic fit: simplified fourier to find the coeficients
 * of sines and cosines of increasing frequencies to fit the
 * data.
 *   A = | 1 cos(2PI*frac) sin(2PI*frac) ... cos([2n]PI*frac) sin([2n]PI*frac) |
 *   b = | yn |
 * With 'frac' the fraction of total period in the X part:
 *   frac = (x[i] - x[0]) / (x[n] - x[0])
 * For aproximation:
 *   c1 + ... +  c[2n-1] * cos([2n]PI) + c[2n] * sin([2n]PI)
 */
class PeriodicFit : public Fit {
  range_t limits;
  unsigned degree;
public:
  PeriodicFit(const points_t &P, unsigned degree);
  unsigned getDegree() { return degree; }
};

enum class LogFitType {
  EXP, /* Exponential: f(x) = c0 * e^(c1 * x)
        * approximates to that line, then re-compute via exp().
        *   A = | 1 ln(x1) |
        *       |    ...   |
        *       | 1 ln(xn) |
        *   b = | ln(yn) |
        * For aproximation:
        *   ln(c1) + c2*x
        */
  PWR, /* Power: f(x) = c0 * x ^ c1
        * approximates to that line, then re-compute via exp().
        *   A = | 1 ln(x1) |
        *       |   ...    |
        *       | 1 ln(xn) |
        *   b = | ln(yn) |
        * For aproximation:
        *   ln(c1) + c2*ln(x)
        */
  HL   /* HalfLife: f(x) = c0 * x * e^(c1 * x)
        * approximates to that line, then re-compute via exp().
        *   A = | 1 x1 |
        *       |  ..  |
        *       | 1 xn |
        *   b = | ln(yn) - ln(xn) |
        * For aproximation:
        *   ln(c1) + ln(x) + c2*x
        */
};
class LogarithmFit : public Fit {
  LogFitType type;
public:
  LogarithmFit(const points_t &P, LogFitType type);
  LogFitType getType() { return type; }
};

// TODO: Use the Gauss-Newton method for non-linear equations

/*
 * Best Fit: Tries all possible fits and returns the one
 * with the least error (for now, polynomial only).
 */
class BestFit {
  const unsigned PolyMax = 3;
  const unsigned PeriMax = 3;
  std::vector<Fit*> fits;
  Fit *best;
public:
  BestFit(const points_t &P);
  ~BestFit();
  Fit *getBest();
};

// Helper
std::string PrintEquation(Fit *f);

} // namespace numcalc

#endif // BESTFIT_H_
